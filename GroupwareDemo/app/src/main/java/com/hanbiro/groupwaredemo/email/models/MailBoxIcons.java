package com.hanbiro.groupwaredemo.email.models;

import android.content.Context;

import com.hanbiro.groupwaredemo.R;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by MinhNguyen on 3/17/15.
 */
public class MailBoxIcons {
    private Map<String, Integer> map = new HashMap<>(10);

    public MailBoxIcons(){
        map.put("Maildir", R.string.mail_ic_inbox);
        map.put("Secure", R.string.mail_ic_secret);
        map.put("Storage", R.string.mail_ic_archive);
        map.put("Sent", R.string.mail_ic_sent);
        map.put("External", R.string.mail_ic_fetching);
        map.put("Receive", R.string.mail_ic_receipt);
        map.put("Temp", R.string.mail_ic_drafts);
        map.put("Spam", R.string.mail_ic_spam);
    }

    public Integer getIcon(String key){
        return map.get(key);
    }

}
