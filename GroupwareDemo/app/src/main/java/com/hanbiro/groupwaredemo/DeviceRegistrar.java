/*
 * Copyright 2010 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hanbiro.groupwaredemo;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;


import com.hanbiro.groupwaredemo.login.LoginActivity;
import com.hanbiro.groupwaredemo.utils.Log;
import com.hanbiro.groupwaredemo.utils.Prefs;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Register/unregister with the Chrome to Phone App Engine server.
 */
public class DeviceRegistrar 
{
	public static final String STATUS_EXTRA = "Status";
	public static final int REGISTERED_STATUS = 1;
	public static final int AUTH_ERROR_STATUS = 2;
	public static final int UNREGISTERED_STATUS = 3;
	public static final int AUTH_ERROR_3_STATUS = 4;
	public static final int ERROR_STATUS = 4;

	private static final String TAG = "DeviceRegistrar";
	public static final String SENDER_ID = "xycrossi@gmail.com";
	public static final String REGISTER_SERVER_LINK = "/eapproval/?mod=token";


	public static void registerWithServer(final Context context, final String deviceRegistrationID) 
	{
		if(Log.DEBUG) Log.v("DeviceRegistrar.registerWithServer()");

		new Thread(new Runnable() 
		{
			public void run() 
			{
				Intent updateUIIntent = new Intent("com.Hanbiro.GlobalGroupware.UPDATE_UI");
				try 
				{

					String strCode = makeRequest(context, deviceRegistrationID);
					int res = Integer.parseInt(strCode);
					if (res == 0) 
					{
						if(Log.DEBUG) Log.v("REGISTERED_STATUS");
						SharedPreferences settings = Prefs.get(context);
						SharedPreferences.Editor editor = settings.edit();
						editor.putString("PushRegistrationID", deviceRegistrationID);
						editor.commit();
						updateUIIntent.putExtra(STATUS_EXTRA, REGISTERED_STATUS);
					}
					else if (res == 400) 
					{
						if(Log.DEBUG) Log.v("AUTH_ERROR_STATUS");
						updateUIIntent.putExtra(STATUS_EXTRA, AUTH_ERROR_STATUS);
					}
					else 
					{
						if(Log.DEBUG) Log.v("AUTH_ERROR_STATUS");
						if(Log.DEBUG) Log.v("Registration error " + Integer.toString(res));
						updateUIIntent.putExtra(STATUS_EXTRA, ERROR_STATUS);
					}


					context.sendBroadcast(updateUIIntent);
				} 
				catch (Exception e) 
				{
					if(Log.DEBUG) Log.v("Registration error " + e.getMessage());
					updateUIIntent.putExtra(STATUS_EXTRA, ERROR_STATUS);
					context.sendBroadcast(updateUIIntent);
				}
			}
		}).start();
	}

	public static void unregisterWithServer(final Context context) 
	{
		if (LoginActivity.flagPush) {
			return;
		}
		
		
		if(Log.DEBUG) Log.v("DeviceRegistrar.unregisterWithServer()");		

		new Thread(new Runnable() 
		{
			public void run() 
			{
				Intent accountResetIntent = new Intent("com.Hanbiro.GlobalGroupware.Config.ACCOUNT_RESET");

				try 
				{
					String strCode = makeRequest(context, "");
					if(Log.DEBUG) Log.v("->"+strCode);
					int res = Integer.parseInt(strCode);
					if (res == 0) {
						SharedPreferences settings = Prefs.get(context);
						SharedPreferences.Editor editor = settings.edit();
						editor.remove("PushRegistrationID");
						editor.remove("AutoLoginKey");
//						editor.remove("LoginId");
//						editor.remove("LoginPassword");
//						editor.remove("Domain");
						AppSetting.getInstance(context).reset(context);
						editor.remove("autologin");
						editor.remove("DeviceId");
						editor.commit();
						accountResetIntent.putExtra(STATUS_EXTRA, UNREGISTERED_STATUS);
					}
					else 
					{
						if(Log.DEBUG) Log.v("Unregistration error " + Integer.toString(res));
						accountResetIntent.putExtra(STATUS_EXTRA, ERROR_STATUS);
					}
				} 
				catch (Exception e) 
				{
					accountResetIntent.putExtra(STATUS_EXTRA, ERROR_STATUS);
					if(Log.DEBUG) Log.v("Unegistration error " + e.getMessage());
				}

				context.sendBroadcast(accountResetIntent);
			}
		}).start();
	}

	private static String makeRequest(Context context,  final String deviceRegistrationID) throws ClientProtocolException, IOException  
	{
//		if(Log.DEBUG) Log.v("DeviceRegistrar.makeRequest()");
//
//		AppSetting appSetting = AppSetting.getInstance(context);
//
//		String login_id = appSetting.getUserId();
//		String login_domain = appSetting.getDomain();//settings.getString("Domain", "");
//		String login_passwd = appSetting.getPassword();//settings.getString("LoginPassword", "");
//		SharedPreferences settings = Prefs.get(context);
//		String strDeviceId = settings.getString("DeviceId", "");
//
//		if(Log.DEBUG) {
//			Log.v("DeviceRegistrar.makeRequest() LoginId = " + login_id);;
//			Log.v("DeviceRegistrar.makeRequest() Domain = " + login_domain);;
//			Log.v("DeviceRegistrar.makeRequest() login_passwd = " + login_passwd);;
//			Log.v("DeviceRegistrar.makeRequest() strDeviceId = " + strDeviceId);;
//
//		}
//
//		List <NameValuePair> postData = new ArrayList<NameValuePair>();
//		postData.add(new BasicNameValuePair("userId", login_id));
//		postData.add(new BasicNameValuePair("passwd", login_passwd));
//		postData.add(new BasicNameValuePair("device", "android"));
//		postData.add(new BasicNameValuePair("token", deviceRegistrationID));
//		postData.add(new BasicNameValuePair("deviceid", strDeviceId));
//		postData.add(new BasicNameValuePair("model", Build.MODEL+"_"+Build.PRODUCT+"_"+Build.BRAND));
//
//
//		HttpPost httpPost = RequestBuilder.buildRaw(login_domain, REGISTER_SERVER_LINK, postData);
//		HttpClient httpClient = new AppEngineClient(context.getApplicationContext())
//                .getNewHttpClient();
//		HttpResponse response = httpClient.execute(httpPost);
//
//		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//		DataInputStream inputStream = new DataInputStream(response.getEntity().getContent());
//		String result = null;
//		byte[] buffer = new byte[1024];
//		int n = 0;
//
//		try {
//			while ((n = inputStream.read(buffer)) != -1) {
//				byteArrayOutputStream.write(buffer, 0, n);
//			}
//
//
//			result = byteArrayOutputStream.toString();
//		} catch (IOException e) {
//
//			e.printStackTrace();
//		}finally{
//			//release resource
//			if(byteArrayOutputStream != null)
//				byteArrayOutputStream.close();
//
//			if(inputStream != null)
//				inputStream.close();
//		}
//
//		//		strUrl = "https://"+login_domain+"/eapproval/?mod=token";
//		//		//strUrl = "http://"+login_domain+"/winapp/song/reg.php";
//		//		strParameter = URLEncoder.encode("userId", "UTF-8") + "=" +URLEncoder.encode(login_id, "UTF-8");
//		//		strParameter += "&" + URLEncoder.encode("passwd", "UTF-8") + "=" +URLEncoder.encode(login_passwd, "UTF-8");
//		//		strParameter += "&" + URLEncoder.encode("device", "UTF-8") + "=" +URLEncoder.encode("android", "UTF-8");
//		//		strParameter += "&" + URLEncoder.encode("token", "UTF-8") + "=" +URLEncoder.encode(deviceRegistrationID, "UTF-8");
//		//		strParameter += "&" + URLEncoder.encode("deviceid", "UTF-8") + "=" +URLEncoder.encode(strDeviceId, "UTF-8");
//		//		strParameter += "&" + URLEncoder.encode("model", "UTF-8") + "=" +URLEncoder.encode(Build.MODEL+"_"+Build.PRODUCT+"_"+Build.BRAND, "UTF-8");
//		//		if(Log.DEBUG) Log.v(strUrl);
//
//		return result;

        return null;
	}
}