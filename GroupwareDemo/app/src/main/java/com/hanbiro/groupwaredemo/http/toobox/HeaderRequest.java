package com.hanbiro.groupwaredemo.http.toobox;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by MinhNguyen on 3/14/15.
 */
public abstract class HeaderRequest<T> extends Request<T> {


    private Map<String, String> headers = new HashMap<String, String>();

    public HeaderRequest(int method, String url, Response.ErrorListener listener) {
        super(method, url, listener);
    }

    public void addHeader(String key, String param){
        headers.put(key, param);
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return headers;
    }



}
