package com.hanbiro.groupwaredemo.email.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;


public class MailBoxItemModel implements Parcelable{


    @SerializedName("new")
	private int newMail;
	private int total;
    private int icon;
	private String name;
	private String key;

	private boolean isSelected = false;


	public MailBoxItemModel(){}

    @Override
    public boolean equals(Object o) {
        if(o instanceof MailBoxItemModel){
            if(this.key != null)
                return this.key.equals(((MailBoxItemModel) o).getKey());
        }
        return super.equals(o);
    }

    public MailBoxItemModel(Parcel parcel){
		newMail = parcel.readInt();
		total = parcel.readInt();
		name = parcel.readString();
		key = parcel.readString();
	}
	
	public static final Creator<MailBoxItemModel> CREATOR = new Creator<MailBoxItemModel>() {
		
		@Override
		public MailBoxItemModel[] newArray(int size) {
			return new MailBoxItemModel[size];
		}
		
		@Override
		public MailBoxItemModel createFromParcel(Parcel source) {
			return new MailBoxItemModel(source);
		}
	};
	
	@Override
	public int describeContents() {
		return 0;
	}
	@Override
	public void writeToParcel(Parcel parcel, int flags) {
		parcel.writeInt(newMail);
		parcel.writeInt(total);

		parcel.writeString(name);
		parcel.writeString(key);
	}
	

	public int getNewMail() {
		return newMail;
	}
	public void setNewMail(int newMail) {
		this.newMail = newMail;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}

	public boolean isSelected() {
		return isSelected;
	}
	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }
}
