package com.hanbiro.groupwaredemo.utils;

import android.content.Context;
import android.telephony.TelephonyManager;

import java.util.UUID;

public class UniqueId {
	
	public static String GetUniqueId(Context context) 
	{
		TelephonyManager tManager = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE); 

	    final String tmDevice, tmSerial, androidId; 
	    
	    tmDevice = "" + tManager.getDeviceId(); 
	    tmSerial = "" + tManager.getSimSerialNumber(); 
	    androidId = "" + android.provider.Settings.Secure.getString(context.getContentResolver(), android.provider.Settings.Secure.ANDROID_ID); 
	 
	    UUID deviceUuid = new UUID(androidId.hashCode(), ((long)tmDevice.hashCode() << 32) | tmSerial.hashCode()); 
	    String deviceId = deviceUuid.toString(); 

	    return deviceId;
	}
}
