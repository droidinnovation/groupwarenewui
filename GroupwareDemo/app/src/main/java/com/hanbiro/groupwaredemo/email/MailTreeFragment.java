package com.hanbiro.groupwaredemo.email;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.gc.materialdesign.views.ButtonFloat;
import com.hanbiro.groupwaredemo.R;
import com.hanbiro.groupwaredemo.email.models.MailBoxItemModel;
import com.hanbiro.groupwaredemo.http.RequestPool;
import com.hanbiro.groupwaredemo.base.ProgressableFragment;
import com.hanbiro.groupwaredemo.email.models.MailBoxModel;
import com.hanbiro.groupwaredemo.email.views.MailBoxAdapter;

/**
 * Created by MinhNguyen on 3/17/15.
 */
public class MailTreeFragment extends ProgressableFragment implements
        SwipeRefreshLayout.OnRefreshListener, AdapterView.OnItemClickListener{

    public static interface TreeDelegate{
        public void onTreeItemClicked(MailBoxItemModel item);
    }

    public static MailTreeFragment createInstance(){
        return new MailTreeFragment();
    }
    private static final String TAG = "mailbox";
    private ListView listView;
    private SwipeRefreshLayout swLayout;
    private RequestManager requestManager;
    private MailBoxModel model;
    private MailBoxAdapter adapter;
    private TreeDelegate delegate;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        requestManager = new RequestManager(activity, this);
        setRetainInstance(true);
        delegate = (TreeDelegate) activity;
    }

    @Override
    public View onCreateMyView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.mail_box_layout, null);
        swLayout = (SwipeRefreshLayout) view.findViewById(R.id.swLayout);
        listView = (ListView) view.findViewById(R.id.listView);
        listView.setOnItemClickListener(this);
        swLayout.setOnRefreshListener(this);
        swLayout.setOnRefreshListener(this);

        view.findViewById(R.id.buttonFloat).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MailWriteActivity.startMailWriteActivity(getActivity());
            }
        });

        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(model == null)
            sendRequest(true, true);
        else
            setModel(model);

    }

    private void setModel(MailBoxModel model) {
        if(model != null) {
            adapter = new MailBoxAdapter(model.getMailbox(), getActivity());
            listView.setAdapter(adapter);
            setViewShow(true);
            if(this.model == null && model.getMailbox() != null && model.getMailbox().size() > 0){
                listView.setSelection(0);
            }
            this.model = model;
        }
    }

    private void sendRequest(boolean cache, boolean progress){
        Request request = requestManager.mailBox(listener);
        request.setShouldCache(cache);
        request.setTag(TAG);
        sendRequest(request, progress);
    }

    private Response.Listener<MailBoxModel> listener = new Response.Listener<MailBoxModel>() {
        @Override
        public void onResponse(MailBoxModel response) {
            if(response != null){
                response.init();
                setModel(response);
                swLayout.setRefreshing(false);
            }

        }
    };

    @Override
    public void onRefresh() {
        sendRequest(false, false);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getRequestPool().getRequestQueue().cancelAll(TAG);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        delegate.onTreeItemClicked(model.getMailbox().get(position));
    }
}
