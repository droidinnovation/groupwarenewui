package com.hanbiro.groupwaredemo.email;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.hanbiro.groupwaredemo.AppSetting;
import com.hanbiro.groupwaredemo.R;
import com.hanbiro.groupwaredemo.email.models.MailBoxItemModel;
import com.hanbiro.groupwaredemo.email.models.MailBoxModel;
import com.hanbiro.groupwaredemo.email.models.MailListModel;
import com.hanbiro.groupwaredemo.http.RequestBuilder;

import org.apache.http.message.BasicNameValuePair;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by MinhNguyen on 3/17/15.
 */
public class RequestManager {
    private static final String LINK_GET_DATA_EMAIL = "/cgi-bin/NEW/mailTohtml5.do";


    private Context context;
    private Response.ErrorListener errorListener;
    public RequestManager(Context context, Response.ErrorListener errorListener){
        this.context = context;
        this.errorListener = errorListener;
    }


    public Request<MailBoxModel> mailBox(Response.Listener<MailBoxModel> listener) {
        RequestBuilder<MailBoxModel> builder = new RequestBuilder<>();
        builder.setListener(listener);
        builder.setErrorListener(errorListener);
        builder.setHttpType(Request.Method.POST);
        builder.setUrl(LINK_GET_DATA_EMAIL);
        builder.setContext(context);
        builder.setJavaClass(MailBoxModel.class);
        Map<String, String> params = new HashMap<>(3);
        params.put("act", "mailbox");
        params.put("HANBIRO_GW", AppSetting.getInstance(context).getCookie());
        builder.setParams(params);

        return builder.build();
    }

    public Request<MailBoxModel> mailList(Response.Listener<MailListModel> listener,
                                          String key, int start) {
        RequestBuilder<MailListModel> builder = new RequestBuilder<>();
        builder.setListener(listener);
        builder.setErrorListener(errorListener);
        builder.setHttpType(Request.Method.POST);
        builder.setUrl(LINK_GET_DATA_EMAIL);
        builder.setContext(context);
        builder.setJavaClass(MailListModel.class);
        Map<String, String> params = new HashMap<>(3);

        params.put("act", "maillist");
        params.put("acl", key);
        params.put("viewcont", start +"," + context.getResources().getInteger(R.integer.item_per_page));
        params.put("HANBIRO_GW", AppSetting.getInstance(context).getCookie());
        builder.setParams(params);

        return builder.build();
    }

}
