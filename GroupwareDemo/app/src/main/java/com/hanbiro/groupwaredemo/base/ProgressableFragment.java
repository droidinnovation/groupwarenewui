/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hanbiro.groupwaredemo.base;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.hanbiro.groupwaredemo.R;
import com.hanbiro.groupwaredemo.http.RequestPool;


public class ProgressableFragment extends BaseFragment implements Response.ErrorListener {

    private View view;
    private ProgressBar mProgressbar;
    private TextView mStandardEmptyView;
    boolean mViewShow;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

        final Context context = getActivity();

        RelativeLayout root = new RelativeLayout(context);

        mProgressbar = new ProgressBar(context, null,
                android.R.attr.progressBarStyleLarge);
        mProgressbar.setId(R.id.progressbar_id);

        mProgressbar.getIndeterminateDrawable().setColorFilter(
                getResources().getColor(R.color.apptheme_color), android.graphics.PorterDuff.Mode.MULTIPLY);

        LayoutParams layoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
        root.addView(mProgressbar, layoutParams);

        //textView
        mStandardEmptyView = new TextView(getActivity());
        mStandardEmptyView.setId(R.id.tv_empty_id);
        layoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        layoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
        root.addView(mStandardEmptyView, layoutParams);

        //view
        view = onCreateMyView(inflater, container, savedInstanceState);

        if(view != null) {
            layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
            layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
            root.addView(view, layoutParams);
        }

        return root;
    }



    public void setEmptyText(String text){
        if(mStandardEmptyView != null)
            mStandardEmptyView.setText(text);
    }

    public void setEmptyText(int text){
        if(mStandardEmptyView != null)
            mStandardEmptyView.setText(text);
    }




    public View onCreateMyView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return null;
    }

    /**
     * Attach to list view once the view hierarchy has been created.
     */
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    /**
     * Detach from list view.
     */
    @Override
    public void onDestroyView() {
        view = null;
        mViewShow = false;
        mStandardEmptyView = null;
        super.onDestroyView();
    }

//    /**
//     * Provide the cursor for the list view.
//     */
//    public void setModel(Model model) {
//        boolean hadAdapter = mAdapter != null;
//        mAdapter = adapter;
//        if (view != null) {
//            view.setAdapter(adapter);
//            if (!mViewShow && !hadAdapter) {
//                // The list was hidden, and previously didn't have an
//                // adapter.  It is now time to show it.
//                setViewShow(true, getView().getWindowToken() != null);
//            }
//        }
//    }



    /**
     * Get the activity's list view widget.
     */
    public View getView() {

        return view;
    }


    public void setViewShow(boolean shown) {
        setViewShown(shown, true);
    }
    

    public void setViewShownNoAnimation(boolean shown) {
        setViewShown(shown, false);
    }
    

    private void setViewShown(boolean shown, boolean animate) {


        if (mViewShow == shown) {
            return;
        }
        mViewShow = shown;

        if (shown) {
            if (animate) {
                mProgressbar.startAnimation(AnimationUtils.loadAnimation(
                        getActivity(), android.R.anim.fade_out));
                view.startAnimation(AnimationUtils.loadAnimation(
                        getActivity(), android.R.anim.fade_in));
            } else {
                mProgressbar.clearAnimation();
                view.clearAnimation();
            }
            mProgressbar.setVisibility(View.GONE);
            view.setVisibility(View.VISIBLE);
        } else {
            if (animate) {
                mProgressbar.startAnimation(AnimationUtils.loadAnimation(
                        getActivity(), android.R.anim.fade_in));
                view.startAnimation(AnimationUtils.loadAnimation(
                        getActivity(), android.R.anim.fade_out));
            } else {
                mProgressbar.clearAnimation();
                view.clearAnimation();
            }
            mProgressbar.setVisibility(View.VISIBLE);
            view.setVisibility(View.GONE);
        }
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }


    @Override
    public void onErrorResponse(VolleyError error) {
        setEmptyText(R.string.alert_network_error_message);
        setViewShow(false);
    }

    protected void sendRequest(Request request, boolean progress){
        setViewShow(!progress);
        RequestPool.getInstance(getActivity()).addToRequestQueue(request);
    }

    protected void sendRequest(Request request){
        sendRequest(request, true);
    }

    protected RequestPool getRequestPool(){
        return RequestPool.getInstance(getActivity());
    }
}
