package com.hanbiro.groupwaredemo.http;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.hanbiro.groupwaredemo.AppSetting;
import com.hanbiro.groupwaredemo.http.toobox.GsonRequest;
import com.hanbiro.groupwaredemo.http.toobox.HeaderRequest;

import java.util.Map;

/**
 * Created by MinhNguyen on 3/14/15.
 */
public class RequestBuilder<T> {
    public static final int TYPE_GSON = 1;

    public static final String DEVICE = "phone";
    public static final String LINK_FORMAT_HTTPS = "http://%s%s";
    public static final String LINK_FORMAT_HTTP = "http://%s%s";

    private int type = TYPE_GSON;
    private int httpType;
    private String url;
    private Map<String, String> params;
    private Context context;
    private Response.Listener<T> listener;
    private String requestBody;
    private Class<T> mJavaClass;
    private String tag;
    private Response.ErrorListener errorListener;

    private void reset(){
        type = 0;
        httpType = -1;
        url = null;
        params = null;
        listener = null;
        requestBody = null;
        mJavaClass = null;
        errorListener = null;
    }

    public Request build(){
        HeaderRequest result = null;
        AppSetting appSetting = AppSetting.getInstance(context);
        String link = String.format(LINK_FORMAT_HTTPS, appSetting.getDomain(), this.url);

        switch (type){
            case TYPE_GSON:{
                result = new GsonRequest(httpType,
                        link, mJavaClass, requestBody, listener,  errorListener);
                ((GsonRequest)result).setParams(params);
                result.setTag(tag);
            }
        }




//        if(appSetting.getCookie() != null && appSetting.getCookie().trim().length() > 0) {
//            result.addHeader("Cookie", "hmail_key=" + appSetting.getHmailKey() + "; HANBIRO_GW=" + appSetting.getCookie());
//            result.addHeader("content-type", "application/json");
//            result.addHeader("User_Agent", "Android APP_HANBIRO_2.0.0.2");
//            result.addHeader("mlang", OSSetting.getLanguageCode());
//            result.addHeader("device", DEVICE);
//        }

        return result;
    }


    public void setType(int type) {
        this.type = type;
    }

    public void setHttpType(int httpType) {
        this.httpType = httpType;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setParams(Map<String, String> params) {
        this.params = params;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void setListener(Response.Listener<T> listener) {
        this.listener = listener;
    }

    public void setRequestBody(String requestBody) {
        this.requestBody = requestBody;
    }

    public void setJavaClass(Class<T> mJavaClass) {
        this.mJavaClass = mJavaClass;
    }

    public void setErrorListener(Response.ErrorListener errorListener) {
        this.errorListener = errorListener;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
}
