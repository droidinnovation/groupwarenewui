package com.hanbiro.groupwaredemo.login.models;

import android.os.Parcel;
import android.os.Parcelable;

public class HomeItemModel implements Parcelable{
	private String num;
	private boolean show;
	private String name;
	private String title;
	private boolean checked;
	
	public HomeItemModel(){}
	public HomeItemModel(Parcel parcel){
		name = parcel.readString();
		num = parcel.readString();
		title = parcel.readString();
		show = (parcel.readByte() == 1);
	}
	
	public static final Creator<HomeItemModel> CREATOR = new Creator<HomeItemModel>() {
		
		@Override
		public HomeItemModel[] newArray(int size) {
			return new HomeItemModel[size];
		}
		
		
		@Override
		public HomeItemModel createFromParcel(Parcel source) {
			// TODO Auto-generated method stub
			return new HomeItemModel(source);
		}
	};
	
	@Override
	public int describeContents() {
		return 0;
	}
	
	
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(name);
		dest.writeString(num);
		dest.writeString(title);
		dest.writeByte((byte)(show?1:0));
	}
	
	public int getNum() {
		if(num != null){
			num = num.replace(",", "");
			return Integer.parseInt(num);
		}
		return 0;
	}
	public void setNum(int num) {
		this.num = num +"";
	}
	public boolean isShow() {
		return show;
	}
	public void setShow(boolean show) {
		this.show = show;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isChecked() {
		return checked;
	}
	public void setChecked(boolean checked) {
		this.checked = checked;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}



}
