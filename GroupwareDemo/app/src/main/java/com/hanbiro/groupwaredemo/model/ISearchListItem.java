package com.hanbiro.groupwaredemo.model;

public interface ISearchListItem {
	public static final int TYPE_BOARD = 1;
	public static final int TYPE_TASK = 2;
	public static final int TYPE_MAIL = 3;
	public static final int TYPE_CIRCULAR = 4;
	
	public String getSubject();
	public String getData1();
	public String getData2();
	
	
}
