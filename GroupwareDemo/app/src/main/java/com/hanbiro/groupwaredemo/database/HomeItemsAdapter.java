package com.hanbiro.groupwaredemo.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class HomeItemsAdapter {
	public static final String TAG = "DatabaseCategoryAdapter";
	
	public static final String TABLE = "HOME_ITEM";
	public static final String KEY_ROWID = "_id";
	public static final String KEY_NAME = "name";
	public static final String KEY_TITLE = "title";
	public static final String KEY_NUM = "num";
	public static final String KEY_SHOW = "show";
	
	
	private Context m_Context;
	private DataBaseHelper m_DBHelper;
	private SQLiteDatabase m_Database;
	
	public HomeItemsAdapter(Context context) {
		m_Context = context;
	}
	
	public synchronized HomeItemsAdapter open(){
		if(m_DBHelper == null){
			m_DBHelper = DataBaseHelper.getInstance(m_Context.getApplicationContext());
		}
		
		m_Database = m_DBHelper.getWritableDatabase();
		return this;
	}
	
	public void deleteAll()
	{
		m_Database.delete(TABLE, null, null);

	}
	
	public void close(){
		//m_Database.close();
		//m_DBHelper.close();
	}
	
	public void insertHomeItem(String name, int num, String title, boolean isShow){
		
		ContentValues values = createCategoryValue(name, num, title, isShow);
		m_Database.insert(TABLE, null, values);
	}
	
	
	public Cursor fetchAllFavorite() {		
		Cursor cursor = m_Database.query(TABLE, new String[] {KEY_ROWID, KEY_NAME, KEY_TITLE,
				KEY_NUM, KEY_SHOW}, null, null, null, null, null);
		cursor.moveToFirst();
		return cursor;
	}
	
	public Cursor fetchFavorite(long rowID) {
		
		Cursor cursor = m_Database.query(true, TABLE, new String[] { KEY_NAME, KEY_TITLE,
				KEY_NUM, KEY_SHOW}, KEY_ROWID + " = " + rowID, null, null, null, null, null);		
		if(cursor != null){
			cursor.moveToFirst();
		}
		
		return cursor;
	}
	
	public boolean deleteCategory(long rowId) {
		
		return m_Database.delete(TABLE, KEY_ROWID + "=" + rowId, null) > 0;
	}

	private ContentValues createCategoryValue(String name, int num, String title, boolean isShow){
		ContentValues value = new ContentValues();
		value.put(KEY_NAME, name);
		value.put(KEY_NUM, num);
		value.put(KEY_TITLE, title);
		
		value.put(KEY_SHOW, isShow?1:0);		
		return value;
	}
}
