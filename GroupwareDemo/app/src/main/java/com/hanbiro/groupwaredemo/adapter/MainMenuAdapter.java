package com.hanbiro.groupwaredemo.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.hanbiro.groupwaredemo.C;
import com.hanbiro.groupwaredemo.R;
import com.hanbiro.groupwaredemo.login.models.MenuItemModel;

import java.util.List;


/**
 * Created by NguyenQuang on 3/14/15.
 */
public class MainMenuAdapter extends BaseAdapter {

    private List<MenuItemModel> listMenu;
    private Context mContext;
    Typeface fontIcon;

    public MainMenuAdapter(List<MenuItemModel> listMenu, Context context) {
        this.listMenu = listMenu;
        this.mContext = context;
        fontIcon =  Typeface.createFromAsset(context.getAssets(), C.FONT_AWARESOME_FILE );
    }


    @Override
    public int getCount() {
        return listMenu.size();
    }

    @Override
    public Object getItem(int position) {
        return listMenu.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder = null;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_main_menu, null);
            holder = new Holder(convertView);
            holder.tvIcon.setTypeface(fontIcon);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        MenuItemModel item = listMenu.get(position);
        holder.tvName.setText(item.getTitle());

        holder.tvIcon.setText(item.getImage());

        return convertView;
    }

    static class Holder {
        public Holder(View v) {
            tvIcon = (TextView) v.findViewById(R.id.ivMenu);
            tvName = (TextView) v.findViewById(R.id.tvNameMenu);
        }

        public TextView tvIcon;
        public TextView tvName;
    }
}
