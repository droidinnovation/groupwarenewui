package com.hanbiro.groupwaredemo.email.views;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.hanbiro.groupwaredemo.C;
import com.hanbiro.groupwaredemo.R;
import com.hanbiro.groupwaredemo.email.models.MailBoxItemModel;

import java.util.List;

public class MailBoxAdapter extends BaseAdapter{

	private List<MailBoxItemModel> items;
	private LayoutInflater inflater;
    Typeface fontIcon;

	public MailBoxAdapter(List<MailBoxItemModel> data, Context context){
		items = data;
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        fontIcon =  Typeface.createFromAsset(context.getAssets(), C.FONT_AWARESOME_FILE );
	}

	@Override
	public int getCount() {
		return (items == null)? 0 : items.size();
	}

	@Override
	public Object getItem(int position) {
		return items.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Holder holder = null;
		if(convertView == null){
			convertView = inflater.inflate(R.layout.mail_box_item, null);
			holder = new Holder(convertView);
            holder.tvIcon.setTypeface(fontIcon);
			convertView.setTag(holder);
		}else{
			holder = (Holder) convertView.getTag();
		}

		MailBoxItemModel itemModel = items.get(position);
		holder.tvName.setText(itemModel.getName());
        holder.tvIcon.setText(itemModel.getIcon());
        holder.tvCount.setText(itemModel.getNewMail() + "");

		return convertView;
	}


	class Holder{
        public Holder(View view){
            tvName = (TextView) view.findViewById(R.id.tvName);
            tvIcon = (TextView) view.findViewById(R.id.tvIcon);
            tvCount = (TextView) view.findViewById(R.id.tvCount);
        }
		public TextView tvName;
        public TextView tvIcon;
        public TextView tvCount;
	}

}
