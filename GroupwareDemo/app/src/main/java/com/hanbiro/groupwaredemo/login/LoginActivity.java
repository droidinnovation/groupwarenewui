package com.hanbiro.groupwaredemo.login;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.ActionBarActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.hanbiro.groupwaredemo.AppSetting;
import com.hanbiro.groupwaredemo.C;
import com.hanbiro.groupwaredemo.DeviceRegistrar;
import com.hanbiro.groupwaredemo.http.RequestPool;
import com.hanbiro.groupwaredemo.R;
import com.hanbiro.groupwaredemo.database.DBDomainAdapter;
import com.hanbiro.groupwaredemo.database.DBIdAdapter;
import com.hanbiro.groupwaredemo.home.HomeActivity;
import com.hanbiro.groupwaredemo.http.ssl.HttpsTrustManager;
import com.hanbiro.groupwaredemo.http.toobox.GsonRequest;
import com.hanbiro.groupwaredemo.login.models.LoginModel;
import com.hanbiro.groupwaredemo.utils.Log;
import com.hanbiro.groupwaredemo.http.RequestBuilder;
import com.hanbiro.groupwaredemo.utils.UniqueId;
import com.hanbiro.groupwaredemo.utils.WebViewSupporter;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends ActionBarActivity implements LoaderCallbacks<Cursor>, OnClickListener, Response.ErrorListener, Response.Listener<LoginModel> {

	@SuppressWarnings("unused")
	private static final String TAG = "LoginActivity";

	private static final int GET_DATA_ERROR = 4;
	public static final String UPDATE_UI_ACTION = "com.Hanbiro.GlobalGroupware.UPDATE_UI";
	private static final int REQUEST_ID_SEND_LOGIN = 0;

	private static final int DIALOG_ID_WITHOUT_INTERNET_ERROR = 1;
	private static final int DIALOG_ID_REGISTER_C2DM_ERROR = 2;
	private static final int DIALOG_ID_LOGIN_ERROR = 3;



	private static final String LINK_LOGIN_REQUEST = "/gw/login_app.php";
	private static final int DIALOG_ID_NETWORK_ERROR = 0;

	private static final int DOMAIN_LOADER_ID = 1;

	private static final int USER_LOADER_ID = 2;


	private String domain = null;
	private String loginId;
	private String password;

	private boolean autoValue;
	private ProgressDialog mProgress;
	private LoginModel loginModel;
	private AppSetting appSetting;
	private AutoCompleteTextView tvDomain;
	private AutoCompleteTextView tvId;
	private EditText tvPassword;
	private SimpleCursorAdapter adapterDomain;
	private SimpleCursorAdapter adapterId;
	private ImageView ivLogo;

    private GsonRequest<LoginModel> loginRequest;

	// Temp
	public static boolean flagPush = false;
	private static final int REQUEST_REGISTER_PUSH = 333;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
//        ActivityUtils.setLanguage(this);
        HttpsTrustManager.allowAllSSL();
		appSetting = AppSetting.getInstance(getApplicationContext());

		if (!isOnline()) {
			showDialog(DIALOG_ID_WITHOUT_INTERNET_ERROR);
		} else {

//			registerBroadcastPN();

			registerReceiver(mUpdateUIReceiver, new IntentFilter(UPDATE_UI_ACTION));

			if ((getIntent().getFlags() & Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) != 0) {
				finish();
				return;
			}

//			requestWindowFeature(Window.FEATURE_NO_TITLE);
			setContentView(R.layout.login_layout);
			findViewById(R.id.lnAddAccount).setOnClickListener(this);

			initControll();
			initAdapter();
		}
	}


	@Override
	protected void onNewIntent(Intent intent) {
		if (!AppSetting.getInstance(this).isLogIn()) {
			finish();
		} else {
			initControll();
			initAdapter();
		}
	}

	public boolean isOnline() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}

	@SuppressWarnings("deprecation")
	private void initAdapter() {
		adapterDomain = new SimpleCursorAdapter(this,
				android.R.layout.simple_dropdown_item_1line, null,
				new String[] { DBDomainAdapter.KEY_DOMAIN_NAME },
				new int[] { android.R.id.text1 }) {
			public CharSequence convertToString(Cursor cursor) {
				String string = cursor
						.getString(cursor
								.getColumnIndexOrThrow(DBDomainAdapter.KEY_DOMAIN_NAME));
				return string;
			}
		};

		adapterId = new SimpleCursorAdapter(this,
				android.R.layout.simple_dropdown_item_1line, null,
				new String[] { DBIdAdapter.KEY_NAME },
				new int[] { android.R.id.text1 }) {
			public CharSequence convertToString(Cursor cursor) {
				String string = cursor.getString(cursor
						.getColumnIndexOrThrow(DBIdAdapter.KEY_NAME));
				return string;
			}
		};


		tvDomain.setAdapter(adapterDomain);
		tvDomain.setThreshold(1);
		tvId.setAdapter(adapterId);
		tvId.setThreshold(1);

		// init loader
		getSupportLoaderManager().initLoader(DOMAIN_LOADER_ID, null, this);
		getSupportLoaderManager().initLoader(USER_LOADER_ID, null, loaderCallbackId);

	}

	private void initControll() {

		loginId = appSetting.getUserId();
		domain = appSetting.getDomain();

		final SharedPreferences config_prefs = PreferenceManager
				.getDefaultSharedPreferences(getApplicationContext());

		/*
		 * the default value when register account is always false
		 */

		autoValue = config_prefs.getBoolean("autologin", false);

		final ImageView imageViewButton = (ImageView) findViewById(R.id.imageViewButton);

		if (autoValue) {
			imageViewButton.setImageResource(R.drawable.on_button);
		} else {
			imageViewButton.setImageResource(R.drawable.off_button);
		}

		imageViewButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (autoValue) {
					autoValue = false;
					imageViewButton.setImageResource(R.drawable.off_button);
				} else {
					autoValue = true;
					imageViewButton.setImageResource(R.drawable.on_button);
				}

				Editor editor = config_prefs.edit();
				editor.putBoolean("autologin", autoValue);
				editor.commit();


			}
		});

		if (autoValue == true) {
			password = appSetting.getPassword();
		}else{
			AppSetting.getInstance(this).setPassWord("");
		}

		tvDomain = (AutoCompleteTextView) findViewById(R.id.domain);
//		tvDomain.setText("");
		if (domain != "" && domain != null) {
			tvDomain.setText(domain);
		}

		tvId = (AutoCompleteTextView) findViewById(R.id.id);
//		tvId.setText("");
//		if (loginId != null && (!loginId.equals(""))) {
//			tvId.setText(loginId);
//		}

		tvPassword = (EditText) findViewById(R.id.password);
//		tvPassword.setText(password);
//		if (domain != null) {
//			tvPassword.requestFocus();
//		}

		ivLogo = (ImageView) findViewById(R.id.ivLogo);

		if (autoValue && password != null
				&& password.length() > 0) {
			startLogin();
		}


		Button nextButton = (Button) findViewById(R.id.next);
		nextButton.setText(R.string.login_ok);

		nextButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				startLogin();
			}
		});

//		if (addAccount) {
//			ivLogo.setImageResource(R.drawable.login_setting_sync_ic);
//			findViewById(R.id.lnAddAccount).setVisibility(View.INVISIBLE);
//			TextView tvAutoLabel = (TextView) findViewById(R.id.tvAutoLabel);
//			tvAutoLabel.setText(R.string.login_sync_remove_label);
//		}

		tvPassword
		.setOnEditorActionListener(new EditText.OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_SEARCH
						|| actionId == EditorInfo.IME_ACTION_DONE
						|| event.getAction() == KeyEvent.ACTION_DOWN
						&& event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
					startLogin();
					return true;
				}

				return true;
			}
		});

	}

	private void startLogin() {

		domain = tvDomain.getText().toString().trim().replace(" ", "");
		loginId = tvId.getText().toString().trim();
		password = tvPassword.getText().toString();

		domain = domain.replace("https://", "");
		domain = domain.replace("http://", "");
		if (loginId.indexOf("@") != -1) {
			String tmp = loginId;
			loginId = tmp.split("@")[0];
		}


		appSetting.setDomain(domain);
		appSetting.setUserId(loginId);
		appSetting.setPassWord(password);

		sendLoginRequest(loginId, password);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		unregisterReceiver(mUpdateUIReceiver);
//		unregisterReceiver(registerReceiver);
	}

	private void sendLoginRequest(String userId, String password) {
        RequestBuilder<LoginModel> builder = new RequestBuilder<>();

        builder.setContext(this);
        builder.setErrorListener(this);
        builder.setHttpType(Request.Method.POST);
        builder.setUrl(LINK_LOGIN_REQUEST);
        builder.setListener(this);
        builder.setJavaClass(LoginModel.class);
        Map<String, String> params = new HashMap<>(10);
        params.put("mode", "login");
        params.put("userid", userId);
        params.put("passwd", password);
        params.put("device", RequestBuilder.DEVICE);
        String m_strDeviceId = UniqueId.GetUniqueId(getApplicationContext());
        params.put("deviceid", m_strDeviceId);
        builder.setParams(params);

        showLoadingProDialog();
        RequestPool.getInstance(this).addToRequestQueue(builder.build().setShouldCache(true));

	}

	private void showLoadingProDialog() {
		if (mProgress == null || (!mProgress.isShowing())) {
			mProgress = ProgressDialog.show(this, "",
					getResources().getString(R.string.loading_text));
		}
	}

    private boolean deviceHasGoogleAccount(){
        AccountManager accMan = AccountManager.get(this);
        Account[] accArray = accMan.getAccountsByType("com.google");
        return accArray.length >= 1 ? true : false;
    }

	private void dismissProDialog() {
		if (mProgress != null && mProgress.isShowing()) {
			try{
				mProgress.dismiss();
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}

	@Override
	protected Dialog onCreateDialog(int id, Bundle bundle) {

		switch (id) {
		case DIALOG_ID_WITHOUT_INTERNET_ERROR: {
			String strMsg = getResources()
					.getString(R.string.app_network_error);
			String strOK = getResources().getString(android.R.string.ok);

			AlertDialog.Builder bld = new AlertDialog.Builder(this).setMessage(
					strMsg).setPositiveButton(strOK,
							new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,
								int whichButton) {
							android.os.Process.killProcess(android.os.Process
									.myPid());
						}
					});
			return bld.create();
		}

		case DIALOG_ID_REGISTER_C2DM_ERROR:
		case DIALOG_ID_LOGIN_ERROR: {
			String message = bundle.getString("message");
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle(R.string.login_error_title).setMessage(message)
			.setCancelable(false)
			.setPositiveButton(android.R.string.ok, null);
			AlertDialog alert = builder.create();
			return alert;
		}
		case DIALOG_ID_NETWORK_ERROR: {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage(R.string.alert_network_error_message)
			.setTitle(R.string.alert_network_error_title)
			.setCancelable(false)
			.setNegativeButton(android.R.string.ok,
					new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,
						int id) {
					dialog.cancel();
				}
			});
			AlertDialog alert = builder.create();
			return alert;
		}
		default:
			break;
		}

		return super.onCreateDialog(id, bundle);
	}


//
//
//	private void registerToC2dm(String account) {
//		if (Log.DEBUG)
//			Log.v("Groupware.register()");
//		SharedPreferences prefs = Prefs.get(this);
//		Editor editor = prefs.edit();
//
//		String m_strDeviceId = UniqueId.GetUniqueId(getApplicationContext());
//		if (Log.DEBUG)
//			Log.v("device:" + m_strDeviceId);
//
//		editor.putString("DeviceId", m_strDeviceId);
//		editor.commit();
//
//		C2DMessaging.register(this, account);
//	}


	Handler handleResponse = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.arg1) {
			case REQUEST_ID_SEND_LOGIN:
				loginModel = (LoginModel) msg.obj;
				if(loginModel.getOk() == 1){
					loginModel.init();
					AppSetting.getInstance(LoginActivity.this).init(loginModel);
//					if (flagPush) {
//						C2DMessaging.unregister(LoginActivity.this);
//
//						startServicePN();
//						handleDataFromC2MDRegister(DeviceRegistrar.REGISTERED_STATUS);
//
//						SharedPreferences prefs = Prefs.get(getApplicationContext());
//						Editor editor = prefs.edit();
//						editor.putString(Constants.KEY_ID_LAST, loginId);
//						editor.commit();
//
//					} else if(deviceHasGoogleAccount()) {
//                        registerToC2dm(DeviceRegistrar.SENDER_ID);
//                    }else{
//                        handleDataFromC2MDRegister(DeviceRegistrar.REGISTERED_STATUS);
//                    }
				}else{
					Bundle bundle = new Bundle();
					bundle.putString("message", loginModel.getMsg());
					showDialog(DIALOG_ID_LOGIN_ERROR, bundle);
					dismissProDialog();
				}

				//				case LOGIN_REGISTER_ACCOUNT: {
				//					dismissProDialog();
				//					registerAccount(loginId, password, autoValue);
				//					setResult(Activity.RESULT_OK);
				//					finish();
				//					break;
				//				}


				break;
			case GET_DATA_ERROR:
				dismissProDialog();
				showDialog(DIALOG_ID_NETWORK_ERROR);
				break;
			default:
				break;
			}

			// dismissProDialog();
		}
	};

//	@Override
//	public void onHttpCompleted(int id, IRequestItem item, byte[] data) {
//		Message message = Message.obtain();
//		message.arg1 = id;
//
//		switch (id) {
//		case REQUEST_ID_SEND_LOGIN: {
//			String response = new String(data);
//			message.obj = new Gson().fromJson(response, LoginModel.class);
//			break;
//		}
//		case REQUEST_REGISTER_PUSH: {
//			String response = new String(data);
//			quang.pushnotification.util.Log.v("RESPONE: " + response);
//			break;
//		}
//		case Constants.REQUEST_MOVE_USER_PUSH: {
//			String responseDelete = new String(data);
//			if (!responseDelete.equals("0")) {
//				runOnUiThread(new Runnable() {
//					@Override
//					public void run() {
//						showAlert(getResources().getString(R.string.title_push), getResources().getString(R.string.message_push));
//					}
//				});
//			}
//		}
//		break;
//		default:
//			break;
//		}
//		handleResponse.sendMessage(message);
//	}
//
//	@Override
//	public void onHttpError(int id, IRequestItem item) {
//
//		Message msg = Message.obtain();
//		msg.arg1 = GET_DATA_ERROR;
//		handleResponse.sendMessage(msg);
//	}

	private void handleDataFromC2MDRegister(int status) {

//		switch (status) {
//		case DeviceRegistrar.UNREGISTERED_STATUS:
//
//			break;
//		case DeviceRegistrar.ERROR_STATUS:
//		case DeviceRegistrar.AUTH_ERROR_STATUS:
//		case DeviceRegistrar.REGISTERED_STATUS: {
//			// login is successfull : login + register to c2dm
//			appSetting.setLogIn(true);
//			// start groupware activity
//			Intent intent = new Intent(this, GlobalGroupware.class);
//			//intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
//			intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
//			intent.putExtra(GlobalGroupware.KEY_HAS_THOUHGT, loginModel.isToday());
//			startActivityForResult(intent, 1);
//
//			getContentResolver().delete(GroupwareProvider.CONTENT_URI, null, null);
//
//			ContentValues values = AccountAdapter.createValue(loginId, domain);
//			getContentResolver().insert(GroupwareProvider.CONTENT_URI, values);
//
//			WebViewSupporter.setCookie(this);
//			sendLoginBroadCast();
//			//download white list
//			Intent workerService = new Intent(this, WorkingService.class);
//			Request request = new WhiteListRequest();
//			MailItemsAdapter dbAdapter = new MailItemsAdapter(this);
//			dbAdapter.open();
//			dbAdapter.deleteAll();
//			workerService.putExtra(C.KEY_DATA, request);
//			startService(workerService);
//
//			Bundle bundle = new Bundle();
//			bundle.putString(AddAccounter.KEY_COOKIES, appSetting.getCookie());
//			bundle.putString(AddAccounter.KEY_DOMAIN, appSetting.getDomain());
//			bundle.putString(AddAccounter.KEY_HMAIL, appSetting.getHmailKey());
//			bundle.putString(AddAccounter.KEY_PASS, appSetting.getPassword());
//			bundle.putString(AddAccounter.KEY_USER_ID, appSetting.getUserId());
//
//
//			//register account
//			new AddAccounter().loggedInCardDAV(bundle, LoginActivity.this);
//			appSetting.perSistenceData(this);
//            finish();
//			break;
//		}
//
//		default:
//			break;
//		}
//
//		dismissProDialog();

	}

	private void sendLoginBroadCast() {
		Intent intent = new Intent();
		intent.setAction(C.BROADCAST_LOGOUT_URL);
		intent.putExtra(C.KEY_BROADCAST, C.VALUE_LOGIN);
		sendBroadcast(intent);
	}



	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK) {
			finish();
		}
	}

	private final BroadcastReceiver mUpdateUIReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (Log.DEBUG)
				Log.v("select_account");
			handleDataFromC2MDRegister(intent.getIntExtra(
					DeviceRegistrar.STATUS_EXTRA, DeviceRegistrar.ERROR_STATUS));
		}
	};

	@Override
	public Loader<Cursor> onCreateLoader(int arg0, Bundle arg1) {
		DBDomainAdapter adapter = new DBDomainAdapter(this);
		adapter.open();
		Loader<Cursor> loader = adapter.fetchAllViaCursorLoader();
		adapter.close();
		return loader;
	}

	@Override
	public void onLoadFinished(Loader<Cursor> arg0, Cursor cursor) {
		adapterDomain.changeCursor(cursor);
	}

	@Override
	public void onLoaderReset(Loader<Cursor> arg0) {
		adapterDomain.changeCursor(null);
	}

	private LoaderCallbacks<Cursor> loaderCallbackId = new LoaderCallbacks<Cursor>() {

		@Override
		public void onLoaderReset(Loader<Cursor> arg0) {
			adapterId.changeCursor(null);
		}

		@Override
		public void onLoadFinished(Loader<Cursor> arg0, Cursor cursor) {
			adapterId.changeCursor(cursor);
		}

		@Override
		public Loader<Cursor> onCreateLoader(int arg0, Bundle arg1) {
			DBIdAdapter adapter = new DBIdAdapter(LoginActivity.this);
			adapter.open();
			Loader<Cursor> loader = adapter.fetchAllViaCursorLoader();
			adapter.close();
			return loader;
		}
	};

	@Override
	public void onClick(View arg0) {
		switch (arg0.getId()) {
		case R.id.lnAddAccount:
			Intent intent = new Intent(Settings.ACTION_SYNC_SETTINGS);
			intent.putExtra(Settings.EXTRA_AUTHORITIES , new String[]{getString(R.string.AUTHORITY_CALENDAR)
					, getString(R.string.AUTHORITY_CONTACT)});
			startActivity(intent);
			break;

		default:
			break;
		}
	}

	// ************** PUSH NOTIFICATION ***************************
	private BroadcastReceiver registerReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
//			UtilApi.sendPushNoticeRequest(context, appSetting.getUserId(), appSetting.getDomain(), appSetting.getPassword(), LoginActivity.this, REQUEST_REGISTER_PUSH);
		}
	};
//
//	// register success
//	private void registerBroadcastPN() {
//		IntentFilter filter = new IntentFilter();
//		filter.addAction(Constants.ACTION_REGISTER_SUCCESS);
//		this.registerReceiver(registerReceiver, filter);
//	}

//	private void startServicePN() {
//		ServiceManager serviceManager = new ServiceManager(this);
//		SharedPreferences prefs = Prefs.get(getApplicationContext());
//		String userId = prefs.getString(Constants.KEY_ID_LAST, "");
//		if (userId == null || userId.equals("")) {
//		    // dont have any account
//			serviceManager.startService();
//		}else {
//			// login another account
//			// remove
//			UtilApi.requestDeleteUserPush(this,appSetting.getUserId(), appSetting.getDomain(), appSetting.getPassword(), LoginActivity.this, Constants.REQUEST_MOVE_USER_PUSH);
//			// end remove
//			serviceManager.clearAccount();
//			serviceManager.stopServiceNoti();
//			serviceManager.startService();
//		}
//
//	}

//	public void showAlert(String title, String mesage) {
//		AlertDialog alertDialog = null;
//		if (alertDialog == null) {
//			AlertDialog.Builder builder = new AlertDialog.Builder(this);
//			builder.setTitle(title);
//			builder.setMessage(mesage);
//			builder.setCancelable(false);
//			builder.setPositiveButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
//				public void onClick(DialogInterface dialog, int id) {
//					finish();
//				}
//			});
//			builder.setNegativeButton(getResources().getString(R.string.retry), new  DialogInterface.OnClickListener() {
//				@Override
//				public void onClick(DialogInterface dialog, int which) {
//					UtilApi.requestDeleteUserPush(LoginActivity.this, appSetting.getUserId(), appSetting.getDomain(), appSetting.getPassword(), LoginActivity.this, Constants.REQUEST_MOVE_USER_PUSH);
//				}
//			});
//			alertDialog = builder.create();
//		}
//		if (!alertDialog.isShowing()) {
//			alertDialog.show();
//		}
//	}

	// ************** END PUSH NOTIFICATION ***************************


    @Override
    public void onErrorResponse(VolleyError error) {
        Message msg = Message.obtain();
        msg.arg1 = GET_DATA_ERROR;
        dismissProDialog();
        Log.v(TAG, error.toString());
        handleResponse.sendMessage(msg);
    }

    @Override
    public void onResponse(LoginModel response) {
        if(response != null) {
            response.init();

            appSetting.setLogIn(true);
            // start groupware activity
            appSetting.init(response);
            Intent intent = new Intent(this, HomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivityForResult(intent, 1);
            WebViewSupporter.setCookie(this);
            sendLoginBroadCast();
            appSetting.perSistenceData(this);
            finish();
        }
        dismissProDialog();

        //download white list
//        Intent workerService = new Intent(this, WorkingService.class);
//        Request request = new WhiteListRequest();
//        MailItemsAdapter dbAdapter = new MailItemsAdapter(this);
//        dbAdapter.open();
//        dbAdapter.deleteAll();
//        workerService.putExtra(C.KEY_DATA, request);
//        startService(workerService);
//
//        Bundle bundle = new Bundle();
//        bundle.putString(AddAccounter.KEY_COOKIES, appSetting.getCookie());
//        bundle.putString(AddAccounter.KEY_DOMAIN, appSetting.getDomain());
//        bundle.putString(AddAccounter.KEY_HMAIL, appSetting.getHmailKey());
//        bundle.putString(AddAccounter.KEY_PASS, appSetting.getPassword());
//        bundle.putString(AddAccounter.KEY_USER_ID, appSetting.getUserId());
//
//
//        //register account
//        new AddAccounter().loggedInCardDAV(bundle, LoginActivity.this);

    }
}
