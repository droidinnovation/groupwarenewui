package com.hanbiro.groupwaredemo.email;

import android.os.Bundle;

import com.hanbiro.groupwaredemo.C;
import com.hanbiro.groupwaredemo.base.BaseActivity;
import com.hanbiro.groupwaredemo.base.DrawerActivity;
import com.hanbiro.groupwaredemo.email.models.MailBoxItemModel;

/**
 * Created by MinhNguyen on 3/17/15.
 */
public class MailActivity extends DrawerActivity implements MailTreeFragment.TreeDelegate{
    private static final String TAG = "mail";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(savedInstanceState == null){
            MailTreeFragment fragment = MailTreeFragment.createInstance();
            replace(fragment, C.TAG_TREE_FRAGMENT, true, false);
        }


    }

    @Override
    public String getTag() {
        return TAG;
    }


    @Override
    protected String getBaseTitle() {
        int index = getAppSetting().indexOfMenu(TAG);
        return getAppSetting().getMenus().get(index).getTitle();
    }

    @Override
    public void onTreeItemClicked(MailBoxItemModel item) {
        MailListFragment fragment = MailListFragment.createInstance(item);
        replace(fragment, C.TAG_LIST_FRAGMENT, true, true);
    }
}
