package com.hanbiro.groupwaredemo.database;

import java.util.Iterator;


import com.commonsware.cwac.loaderex.acl.SQLiteCursorLoader;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class MailItemsAdapter {
	public static final String TAG = "DatabaseCategoryAdapter";

	public static final String TABLE = "MAIL_LIST";
	public static final String COLUMN__ROWID = "_id";
	public static final String COLUMN_EMAIL = "mail";

	public static final String FECHALL_SELECT = "SELECT " + COLUMN__ROWID + " ," + COLUMN_EMAIL
			+ " FROM " + TABLE;

//	private Context m_Context;
//	private DataBaseHelper m_DBHelper;
//	private SQLiteDatabase m_Database;
//
//	public MailItemsAdapter(Context context) {
//		m_Context = context;
//	}
//
//
//	public SQLiteCursorLoader fetchAllViaCursorLoader(){
//		return new SQLiteCursorLoader(m_Context, m_DBHelper, FECHALL_SELECT, null);
//	}
//
//	public synchronized MailItemsAdapter open(){
//		if(m_DBHelper == null){
//			m_DBHelper = DataBaseHelper.getInstance(m_Context.getApplicationContext());
//		}
//
//		m_Database = m_DBHelper.getWritableDatabase();
//
//
//		return this;
//	}
//
//	public void deleteAll()
//	{
//		m_Database.delete(TABLE, null, null);
//
//	}
//
//	public void close(){
//		//m_Database.close();
//		//m_DBHelper.close();
//	}
//
//
//	public void insert(WhiteListModel listModel){
//		m_Database.beginTransaction();
//		try{
//			if(listModel != null && listModel.getAddrlist() != null){
//				Iterator<String> iterator = listModel.getAddrlist().iterator();
//				while (iterator.hasNext()) {
//					String string = (String) iterator.next();
//					insertItem(string);
//				}
//			}
//			m_Database.setTransactionSuccessful();
//		}finally{
//			m_Database.endTransaction();
//		}
//
//	}
//
//	public void insertItem(String name){
//		ContentValues values = createCategoryValue(name);
//		m_Database.insert(TABLE, null, values);
//	}
//
//
//	public Cursor fetchAll() {
//		Cursor cursor = m_Database.query(TABLE, new String[] {COLUMN__ROWID, COLUMN_EMAIL}, null, null, null, null, null);
//		cursor.moveToFirst();
//		return cursor;
//	}
//
//	private ContentValues createCategoryValue(String name){
//		ContentValues value = new ContentValues();
//		value.put(COLUMN_EMAIL, name);
//		return value;
//	}
}
