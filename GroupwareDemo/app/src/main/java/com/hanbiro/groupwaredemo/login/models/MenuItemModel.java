package com.hanbiro.groupwaredemo.login.models;


import com.hanbiro.groupwaredemo.R;

public class MenuItemModel {
	private String name;
	private String title;
    private int image;
	private int onIcon;
	private int offIcon;

    @Override
    public boolean equals(Object o) {
        if(o instanceof MenuItemModel && name != null){
            return name.equals(((MenuItemModel) o).getName());
        }
        return super.equals(o);
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getOnIcon() {
		return onIcon;
	}
	public void setOnIcon(int onIcon) {
		this.onIcon = onIcon;
	}
	public int getOffIcon() {
		return offIcon;
	}
	public void setOffIcon(int offIcon) {
		this.offIcon = offIcon;
	}
	
	
}
