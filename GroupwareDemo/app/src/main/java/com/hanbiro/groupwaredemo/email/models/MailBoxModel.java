package com.hanbiro.groupwaredemo.email.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.hanbiro.groupwaredemo.R;

import org.apache.commons.lang.math.NumberUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class MailBoxModel implements Parcelable {
	/**
	 * 
	 */
	private static List<String> EXCEPT_FOLDER = new ArrayList<String>();
	static{
//		EXCEPT_FOLDER.add("Trash");
//		EXCEPT_FOLDER.add("Spam");
		EXCEPT_FOLDER.add("Secure");
//		EXCEPT_FOLDER.add("External");
//		
//		//EXCEPT_FOLDER.add("External");
//		EXCEPT_FOLDER.add("Receive");
		
	}
	
	
	private static final long serialVersionUID = 1L;
	private ArrayList<MailBoxItemModel> mailbox;

	
	public ArrayList<MailBoxItemModel> getBoxList(String key){
		ArrayList<MailBoxItemModel> temp = new ArrayList<MailBoxItemModel>();
		Iterator<MailBoxItemModel> iterator = mailbox.iterator();
		
		while (iterator.hasNext()) {
			MailBoxItemModel mailBoxItemModel = (MailBoxItemModel) iterator
					.next();
			
			if(mailBoxItemModel.getKey().equals(key) 
					|| EXCEPT_FOLDER.contains(mailBoxItemModel.getKey()))
				continue;
			
			temp.add(mailBoxItemModel);
//			if(NumberUtils.isNumber(mailBoxItemModel.getKey())){
//				temp.add(mailBoxItemModel);
//			}else{
//				
//			}
		}
		return temp;
	}

    public void init(){
        MailBoxItemModel removeItem = new MailBoxItemModel();
        removeItem.setKey("Trash");
        if(mailbox != null) {
            mailbox.remove(removeItem);
            MailBoxIcons boxIcons = new MailBoxIcons();
            for(MailBoxItemModel item : mailbox){
                Integer icon = boxIcons.getIcon(item.getKey());
                if(icon == null)
                    icon = R.string.mail_ic_folder;

                item.setIcon(icon);
            }
        }

    }

	public ArrayList<MailBoxItemModel> getMailbox() {
		return mailbox;
	}

	public void setMailbox(List<MailBoxItemModel> mailbox) {
		List<MailBoxItemModel> temp = new ArrayList<MailBoxItemModel>();
		this.mailbox = new ArrayList<MailBoxItemModel>();
		Iterator<MailBoxItemModel> iterator = mailbox.iterator();
		while (iterator.hasNext()) {
			MailBoxItemModel mailBoxItemModel = (MailBoxItemModel) iterator
					.next();
			if(NumberUtils.isNumber(mailBoxItemModel.getKey())){
				temp.add(mailBoxItemModel);
			}else{
				this.mailbox.add(mailBoxItemModel);
			}
		}
		Collections.sort(temp, new Comparator<MailBoxItemModel>() {

			@Override
			public int compare(MailBoxItemModel lhs, MailBoxItemModel rhs) {
				return lhs.getName().compareTo(rhs.getName());
			}
		});
		this.mailbox.addAll(temp);
	}

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeSerializable(this.mailbox);
    }

    public MailBoxModel() {
    }

    private MailBoxModel(Parcel in) {
        this.mailbox = (ArrayList<MailBoxItemModel>) in.readSerializable();
    }

    public static final Parcelable.Creator<MailBoxModel> CREATOR = new Parcelable.Creator<MailBoxModel>() {
        public MailBoxModel createFromParcel(Parcel source) {
            return new MailBoxModel(source);
        }

        public MailBoxModel[] newArray(int size) {
            return new MailBoxModel[size];
        }
    };
}
