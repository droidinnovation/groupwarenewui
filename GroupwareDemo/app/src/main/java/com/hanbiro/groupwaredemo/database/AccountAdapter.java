package com.hanbiro.groupwaredemo.database;



import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.commonsware.cwac.loaderex.acl.SQLiteCursorLoader;

public class AccountAdapter {
	public static final String TAG = "DatabaseCategoryAdapter";
	
	
	public static final String TABLE = "ACCOUNT";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_USER_ID = "USER_ID";
	public static final String COLUMN_DOMAIN = "DOMAIN";

	public static final String FECHALL_SELECT = "SELECT " + COLUMN_ID + " ," + COLUMN_USER_ID + ", " + COLUMN_DOMAIN
			+ " FROM " + TABLE;

	public static final String CHECK_EXIST_QUERY = "SELECT 1 from " + TABLE + " where " + COLUMN_USER_ID + " =%s";
	
	private Context m_Context;
	private DataBaseHelper m_DBHelper;
	private SQLiteDatabase m_Database;

	public AccountAdapter(Context context) {
		m_Context = context;
	}

	public synchronized AccountAdapter open(){
		if(m_DBHelper == null){
			m_DBHelper = DataBaseHelper.getInstance(m_Context.getApplicationContext());
		}
		
		m_Database = m_DBHelper.getWritableDatabase();
		return this;
	}

	public int deleteAll()
	{
		return m_Database.delete(TABLE, null, null);
	}

	public void close(){
		//m_Database.close();
		//m_DBHelper.close();
	}

	public boolean exist(ContentValues values) {
		String userId = values.getAsString(COLUMN_USER_ID);
		Cursor cursor = m_Database.rawQuery("select 1 from " + TABLE + " where " + COLUMN_USER_ID + " =?", 
				new String[] {userId});
		boolean exists = (cursor.getCount() > 0);
		cursor.close();
		return exists;
	}
	
	public boolean exist(String id) {
		Cursor cursor = m_Database.rawQuery("select 1 from " + TABLE + " where " + COLUMN_USER_ID + " =?", 
				new String[] {id});
		boolean exists = (cursor.getCount() > 0);
		cursor.close();
		return exists;
	}

	public void insertIfNotExist(String userId, String domain){
		if(!exist(userId)){
			ContentValues values = createValue(userId, domain);
			m_Database.insert(TABLE, null, values);
		}
	}


	public void insertIdItem(String userId, String domain){
		try{
			ContentValues values = createValue(userId, domain);
			m_Database.insert(TABLE, null, values);
		}catch (android.database.sqlite.SQLiteConstraintException se){
			se.printStackTrace();
		}
	}
	
	public long insertIdItem(ContentValues values){
		try{
			
			return m_Database.insert(TABLE, null, values);
		}catch (android.database.sqlite.SQLiteConstraintException se){
			se.printStackTrace();
		}
		
		return 0;
	}

	
	
	public SQLiteCursorLoader fetchAllViaCursorLoader(){
		return new SQLiteCursorLoader(m_Context, m_DBHelper, FECHALL_SELECT, null);
	}

	public Cursor fetchAllAccounts() {		
		Cursor cursor = m_Database.query(TABLE, new String[] {COLUMN_ID, COLUMN_USER_ID, COLUMN_DOMAIN}
			, null, null, null, null, null);
		cursor.moveToFirst();
		return cursor;
	}

	public Cursor fetchId(long rowID) {

		Cursor cursor = m_Database.query(true, TABLE, new String[] { COLUMN_USER_ID, COLUMN_DOMAIN}, COLUMN_ID + " = " + rowID, null, null, null, null, null);		
		if(cursor != null){
			cursor.moveToFirst();
		}

		return cursor;
	}

	public boolean deleteId(long rowId) {

		return m_Database.delete(TABLE, COLUMN_ID + "=" + rowId, null) > 0;
	}

	public static ContentValues createValue(String userId, String domain){
		ContentValues value = new ContentValues();
		value.put(COLUMN_USER_ID, userId);
		value.put(COLUMN_DOMAIN, domain);
		return value;
	}
}
