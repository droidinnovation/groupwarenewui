package com.hanbiro.groupwaredemo.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.hanbiro.groupwaredemo.AppSetting;

public class WebViewSupporter {
	public static void setCookie(Context context){
		AppSetting appSetting = AppSetting.getInstance(context);
		CookieSyncManager cookieSyncManager = CookieSyncManager.createInstance(context);
		CookieManager cookieManager = CookieManager.getInstance();
		cookieManager.setAcceptCookie(true);

		cookieManager.removeAllCookie();

		String cookieString =  "HANBIRO_GW=" + appSetting.getCookie() + "; domain=" + appSetting.getDomain().replace("http://", "");
		String cookieString2 =  "hmail_key=" + appSetting.getHmailKey() + "; domain=" + appSetting.getDomain().replace("http://", "");
		
		cookieManager.setCookie(appSetting.getDomain(), cookieString2);
		cookieManager.setCookie(appSetting.getDomain(), cookieString);
		
		cookieSyncManager.sync();
	}
	
	public static WebViewClient createClient(){
		MyWebViewClient client = new MyWebViewClient();
		return client;
	}
	
}

class MyWebViewClient extends WebViewClient{

	private static final String TAG = "MyWebViewClient";


	@Override
	public void onPageFinished(WebView view, String url) {
		Log.v(TAG, "onPageFinished url: " + url);
		super.onPageFinished(view, url);
		
	}

	@Override
	public void onPageStarted(WebView view, String url, Bitmap favicon) {
		super.onPageStarted(view, url, favicon);
		Log.v(TAG, "onPageStarted url: " + url);
	}

	@Override
	public void onReceivedError(WebView view, int errorCode,
			String description, String failingUrl) {
		Log.v(TAG, "onReceivedError url: " + failingUrl);
		super.onReceivedError(view, errorCode, description, failingUrl);
		
	}


	@Override
	public boolean shouldOverrideUrlLoading(WebView view, String url) {
		Log.v(TAG, "shouldOverrideUrlLoading : " + url);

		return false;
	}

	@Override
	public void onLoadResource(WebView view, String url) {
		Log.v(TAG, "onLoadResource : " + url);
		super.onLoadResource(view, url);
	}

    @Override
    public void onReceivedSslError (WebView view, SslErrorHandler handler, SslError error) {
        handler.proceed();
    }

	
}
