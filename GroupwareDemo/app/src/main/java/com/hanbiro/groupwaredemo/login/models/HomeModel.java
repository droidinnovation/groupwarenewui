package com.hanbiro.groupwaredemo.login.models;



import com.hanbiro.groupwaredemo.C;

import java.util.List;

public class HomeModel {
	private int ok;
	private List<HomeItemModel> data;
	private int total;

	public void init(){
		for(HomeItemModel item : data){
			if(C.getInstance().ABSENT_LIST.contains(item.getName())){
				data.remove(item);
			}
		}
	}


	public int getOk() {
		return ok;
	}


	public void setOk(int ok) {
		this.ok = ok;
	}


	public List<HomeItemModel> getData() {
		return data;
	}
	public void setData(List<HomeItemModel> data) {
		this.data = data;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}


}
