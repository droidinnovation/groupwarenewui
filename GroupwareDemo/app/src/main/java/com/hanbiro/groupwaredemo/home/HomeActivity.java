package com.hanbiro.groupwaredemo.home;

import android.os.Bundle;

import com.hanbiro.groupwaredemo.C;
import com.hanbiro.groupwaredemo.base.BaseActivity;
import com.hanbiro.groupwaredemo.base.DrawerActivity;
import com.hanbiro.groupwaredemo.base.SampleFragment;
import com.hanbiro.groupwaredemo.email.MailTreeFragment;

/**
 * Created by MinhNguyen on 3/17/15.
 */
public class HomeActivity extends DrawerActivity{
    private static final String TAG = "home";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState == null){
            SampleFragment fragment = new SampleFragment();
            replace(fragment, C.TAG_TREE_FRAGMENT, true, false);
        }
    }

    @Override
    public String getTag() {
        return TAG;
    }

    @Override
    protected String getBaseTitle() {
        return getAppSetting().getStrAppTitle();
    }
}
