package com.hanbiro.groupwaredemo.email.models;

import android.content.Context;

import com.hanbiro.groupwaredemo.C;
import com.hanbiro.groupwaredemo.R;
import com.hanbiro.groupwaredemo.model.ISearchListItem;
import com.hanbiro.groupwaredemo.model.ISearchListModel;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MailListModel implements ISearchListModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int total;
	private int set_start;
	private List<MailListItemModel> maillist;
	private int page = 1;


	@Override
	public List<? extends ISearchListItem> getData() {
		return maillist;
	}
	@Override
	public int getPage() {
		return page;
	}
	@Override
	public int setPage(int page) {
		this.page = page;
		return page;
	}
	@Override
	public void addMore(ISearchListModel model) {
		if(maillist == null)
			this.maillist = new ArrayList<MailListItemModel>();
		Iterator<? extends ISearchListItem> iterator = model.getData().iterator();
		while (iterator.hasNext()) {
			MailListItemModel iListItem = (MailListItemModel) iterator.next();
			maillist.add(iListItem);
		}

	}


	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public int getSet_start() {
		return set_start;
	}
	public void setSet_start(int set_start) {
		this.set_start = set_start;
	}

	public boolean hasRemain(){
		return (maillist != null && maillist.size() < total);
	}
	public List<MailListItemModel> getMaillist() {
		return maillist;
	}
	public void setMaillist(List<MailListItemModel> maillist) {
		this.maillist = maillist;
	}

	public void resetChecked(boolean checked){
		if(maillist != null){
			Iterator<MailListItemModel> iterator = maillist.iterator();
			while (iterator.hasNext()) {
				MailListItemModel mailListItemModel = (MailListItemModel) iterator
						.next();
				//if(mailListItemModel.isChecked())
				mailListItemModel.setChecked(checked);

			}
		}
	}

	public void addMore(MailListModel listModel){
		if(listModel == null)
			return;

		this.total = listModel.total;
		if(maillist == null)
			maillist = listModel.maillist;
		else
			maillist.addAll(listModel.maillist);
	}


	public static void saveToFile(Context context, String fileName, MailListModel list){
		try{
			FileOutputStream fos = null;
			ObjectOutputStream os = null;
			try{
				fos = context.openFileOutput(fileName, Context.MODE_PRIVATE);
				//				fos = new FileOutputStream(fileName);
				os = new ObjectOutputStream(fos);
				os.writeObject(list);
			}catch(Exception ex){
				ex.printStackTrace();
			}finally{
				if(os!=null){
					os.flush();
					os.close();
				}
				if(fos != null){
					fos.close();
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}


	public static MailListModel loadFromFile(Context context, String fileName){
		MailListModel result = null;
		try{
			FileInputStream fis = null;
			ObjectInputStream is = null;

			try{
				fis = context.openFileInput(fileName);
				//				fis = new FileInputStream(fileName);
				is = new ObjectInputStream(fis);
				result = (MailListModel) is.readObject();
			}catch(Exception e){
				e.printStackTrace();
			}finally{
				if(is!=null){
					is.close();
				}
				if(fis != null){
					fis.close();
				}
			}

		}catch(Exception ex){
			ex.printStackTrace();
		}

		return result;
	}
	@Override
	public int count() {
		return (maillist == null)? 0: maillist.size();
	}
	
	@Override
	public String getTitle(Context context) {
		return context.getString(R.string.mail);
	}
	
	@Override
	public String getKey() {
		return C.KEY_MAIL;
	}
	
}
