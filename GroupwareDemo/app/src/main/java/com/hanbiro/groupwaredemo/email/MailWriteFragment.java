package com.hanbiro.groupwaredemo.email;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hanbiro.groupwaredemo.AppSetting;
import com.hanbiro.groupwaredemo.R;
import com.hanbiro.groupwaredemo.base.BaseFragment;

/**
 * Created by NguyenQuang on 3/18/15.
 */
public class MailWriteFragment extends BaseFragment {

    public static MailWriteFragment createInstance(Bundle bundle){
        MailWriteFragment fragment = new MailWriteFragment();
        if (bundle!=null){
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        AppSetting appSetting = AppSetting.getInstance(getActivity());
        String email = appSetting.getUserId() + appSetting.getMailDomain();

        View view = inflater.inflate(R.layout.fragment_writemail,container,false);
        TextView tvMailFrom = (TextView) view.findViewById(R.id.tvMailFrom);
        final Button btnMailCCBC = (Button) view.findViewById(R.id.btnMailCCBC);
        final LinearLayout layoutBcc = (LinearLayout) view.findViewById(R.id.layoutMailBcc);
        final LinearLayout layoutCc = (LinearLayout) view.findViewById(R.id.layoutMailCc);

        tvMailFrom.setText(email);
        btnMailCCBC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnMailCCBC.setVisibility(View.GONE);
                layoutBcc.setVisibility(View.VISIBLE);
                layoutCc.setVisibility(View.VISIBLE);
            }
        });

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_write_mail, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);

    }
}
