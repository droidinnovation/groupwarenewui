package com.hanbiro.groupwaredemo.model;

import android.content.Context;

import java.util.List;

public interface ISearchListModel {
	public List<? extends ISearchListItem> getData();
	public int getTotal();
	public int getPage();
	public int count();
	public String getTitle(Context context);
	public String getKey();
	public int setPage(int page);
	public void addMore(ISearchListModel model);
	public boolean hasRemain();
	
}
