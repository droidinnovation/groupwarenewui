package com.hanbiro.groupwaredemo.database;



import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.commonsware.cwac.loaderex.acl.SQLiteCursorLoader;

public class DBDomainAdapter {
	public static final String TAG = "DatabaseCategoryAdapter";

	public static final String TABLE = "DOMAINS";
	public static final String KEY_ROWID = "_id";
	public static final String KEY_DOMAIN_NAME = "domain";

	public static final String FECHALL_SELECT = "SELECT " + KEY_ROWID + " ," + KEY_DOMAIN_NAME
			+ " FROM " + TABLE;

	private Context m_Context;
	private DataBaseHelper m_DBHelper;
	private SQLiteDatabase m_Database;

	public DBDomainAdapter(Context context) {
		m_Context = context;
	}

	public synchronized DBDomainAdapter open(){
		if(m_DBHelper == null){
			m_DBHelper = DataBaseHelper.getInstance(m_Context.getApplicationContext());
		}

		m_Database = m_DBHelper.getWritableDatabase();
		return this;
	}

	public void deleteAll()
	{
		m_Database.delete(TABLE, null, null);

	}

	public void close(){
		//m_Database.close();
		//m_DBHelper.close();
	}


	public void insertIfNotExist(String domain){
		if(!exist(domain)){
			ContentValues values = createCategoryValue(domain);
			m_Database.insert(TABLE, null, values);
		}

	}

	public boolean exist(String id) {
		Cursor cursor = m_Database.rawQuery("select 1 from " + TABLE + " where " + KEY_DOMAIN_NAME + "=?", 
				new String[] {id});
		boolean exists = (cursor.getCount() > 0);
		cursor.close();
		return exists;
	}

	public void insertDomainItem(String domain){
		try{
			ContentValues values = createCategoryValue(domain);
			m_Database.insert(TABLE, null, values);
		}catch (android.database.sqlite.SQLiteConstraintException se){
			//se.printStackTrace();
		}
	}

	public SQLiteCursorLoader fetchAllViaCursorLoader(){
		return new SQLiteCursorLoader(m_Context, m_DBHelper, FECHALL_SELECT, null);
	}

	public Cursor fetchAllFavorite() {		
		Cursor cursor = m_Database.query(TABLE, new String[] {KEY_ROWID, KEY_DOMAIN_NAME}, null, null, null, null, null);
		cursor.moveToFirst();
		return cursor;
	}

	public Cursor fetchDomain(long rowID) {

		Cursor cursor = m_Database.query(true, TABLE, new String[] { KEY_DOMAIN_NAME}, KEY_ROWID + " = " + rowID, null, null, null, null, null);		
		if(cursor != null){
			cursor.moveToFirst();
		}

		return cursor;
	}

	public boolean deleteDomain(long rowId) {

		return m_Database.delete(TABLE, KEY_ROWID + "=" + rowId, null) > 0;
	}

	private ContentValues createCategoryValue(String domain){
		ContentValues value = new ContentValues();
		value.put(KEY_DOMAIN_NAME, domain);
		return value;
	}
}
