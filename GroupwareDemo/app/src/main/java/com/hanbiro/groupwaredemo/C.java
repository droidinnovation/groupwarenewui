package com.hanbiro.groupwaredemo;

import java.util.ArrayList;
import java.util.List;

public class C {


    private static C instance;

    public final List<String> CONFIRMATION_LIST;

    public final List<String> ABSENT_LIST;

    private C() {
        CONFIRMATION_LIST = new ArrayList<String>();
        CONFIRMATION_LIST.add("timecard");
        CONFIRMATION_LIST.add("holiday");
        CONFIRMATION_LIST.add("equip");

        ABSENT_LIST = new ArrayList<String>();
        ABSENT_LIST.add("timecard");
        ABSENT_LIST.add("equip");
    }


    synchronized public static C getInstance() {
        if (instance == null) {
            instance = new C();
        }
        return instance;
    }


    public static final String KEY_BOARD = "Board";
    public static final String KEY_TODO = "Todo";
    public static final String KEY_CIRCULAR = "Circular";
    public static final String KEY_APPROVAL = "Approval";
    public static final String KEY_MAIL = "Mail";
    public static final String KEY_TASK = "Report";

    public static final String MENU_CONTACT = "address";
    public static final String MENU_HOLIDAY = "hr";
    public static final String MENU_MAIL = "mail";
    public static final String MENU_HOME = "home";
    public static final String MENU_BOARD = "board";
    public static final String MENU_TASK1 = "diary";
    public static final String MENU_TASK2 = "task";
    public static final String MENU_TODO = "todo";
    public static final String MENU_HOLIDAY2 = "holiday";

    public static final String MENU_CIRCULAR = "circulation";
    public static final String MENU_APPROVAL = "approval";
    public static final String MENU_WHISPER = "postit";

    public static final String MENU_PROJECT = "project";
    public static final String MENU_SEARCH = "search";
    public static final String MENU_CALENDAR = "calendar";
    public static final String MENU_ALARM = "alarm";
    public static final String MENU_PC = "pc";
    public static final String MENU_ORGANIZATION = "organization";
    public static final String MENU_ARCHIVES = "archives";
    public static final String MENU_LOGOUT = "logout";
    public static final String MENU_EQUIP = "equip";
    public static final String MENU_TIMECARD = "timecard";
    public static final String WEBVIEW_CACHE_FOLDER = "cache3";
    public static final String TAG_FOLDER = C.TAG_EXTRA1;

    public static final String KEY_ALL = "all";
    public static final String KEY_TODO_COMPLETE = "Completed";


    public static final String COMMON_SHARED_PREFERENCE = "F";
    public static final String BROADCAST_LOGOUT_URL = "com.Hanbiro.GlobalGroupware.logout";
    public static final String KEY_BROADCAST = "key_value";
    public static final String VALUE_LOGOUT = "out";
    public static final String VALUE_LOGIN = "in";
    public static final String TAG_TREE_FRAGMENT = "tree";
    public static final String TAG_LIST_FRAGMENT = "list";

    public static final String LINK_CONTACT_IMAGE_OLIST = "/eapproval/?mod=user&req=photo";
    public static final String LINK_CONTACT_IMAGE = "/gw/m.php?do=common_image&type=addrbook&atype=%s&act=zoom&file=%s";
    public static final String LOG_OUT_LINK = "/eapproval/?mod=token";
    public static final String LIKT_TO_OPEN_PC = "http://%s/gw/login_app.php?mode=tunnel&hkey=%s&type=pc";
    public static final String LINK_GET_EDITOR = "/groupware/add/editor/wysiwyg.html?type=android&dt=%s";


    /*----------------------------------------- Calendar ----------------------------------------*/
    public static final String LINK_CALENDAR_TREE = "/gw/m.php?do=calendar_tree";
    public static final String LINK_CALENDAR_MONTH = "/gw/m.php?do=calendar_view";
    public static final String LINK_CALENDAR_DELETE = "/gw/m.php?do=calendar_write";
    public static final String LINK_CALENDAR_CATEGORY = "/gw/m.php?do=calendar_common";
    public static final String LINK_CALENDAR_WRITE = "/gw/m.php?do=calendar_write";
    public static final String LINK_CALENDAR_WRITE_INIT = "/gw/m.php?do=calendar_write&mode=view";
    public static final String LINK_CAL_TIME_ZONE = "/gw/m.php?do=calendar_common&mode=timezone";
    public static final String LINK_WRITE_INIT = "/gw/m.php?do=calendar_write&mode=config&ctype=%s";
    public static final String LINK_PC_FORMAT = "https://%s/mobile/login_app.php?mode=tunnel&type=pc&ver=1&hkey=%s";


    public static final String IMAGE_KEY_FORMAT = "img%s%s";
    public static final String FILES_KEY = "files";
    public static final String SPAM_FOLDER = "Spam";
    public static final String SETTING_SYNC_ID = "setting_sync";
    public static final String SETTING_ID = "setting";

    public static final String KEY_DATA = "data";
    public static final String KEY_DATA1 = "data1";
    public static final String KEY_DATA2 = "data2";
    public static final String TAG_DIALOG = "dialog";
    public static final String TAG_PRO_DIALOG = "prdl";
    public static final String TAG_VIEW = "view";
    public static final String TAG_EXTRA = "extra";
    public static final String TAG_EXTRA1 = "extra1";
    public static final String TAG_EXTRA2 = "extra2";
    public static final String TAG_WRITE = "write";
    public static final String TAG_MENU = "menu";
    public static final String TAG_DOWNLOAD = "d";

    public static final String KEY_CLOUD_DATA = "data";
    public static final String KEY_DAILY_REPORT = "journal";
    public static final String KEY_TASK_REPORT = "report";


    public static final String CHEAT_MIN_WEBVIEW_HEIGHT = "<br /><br /><br /><br /><br /><br /><br /><br />";
    /* Search API */

    public static final String LINK_SEARCH_ALL = "/gw/search.php?keyword=%s";
    public static final String LINK_SEARCH_BOARD = "/gw/?do=board_search&keyword=%s";
    public static final String LINK_SEARCH_TODO = "/gw/?do=todo_search&keyword=%s";
    public static final String LINK_SEARCH_CIRCULAR = "/gw/?do=circular_search&keyword=%s";
    public static final String LINK_SEARCH_APPROVAL = "/eapproval/?m=json&r=search";
    public static final String LINK_SEARCH_TASK = "/report/?m=json&r=search";
    public static final String LINK_SEARCH_MAIL = "/cgi-bin/NEW/mailTohtml5.do?act=maillist&acl=Maildir&viewcont=%s&page=%d&start=%d&limit=%d&mailsort=date&sortkind=0&msgsig=all&keyword=%s&searchbox=all&searchfild=all";
    //	public static final String LINK_SEARCH_MAIL = "/cgi-bin/NEW/mailTohtml5.do?act=maillist&acl=Maildir&viewcont=%s&page=%d&start=%d&limit=%d&mailsort=date&sortkind=0&msgsig=all&keyword=%s&searchbox=Maildir&searchfild=all";
    public static final String LINK_SEARCH_SETTING = "/jqmobile/view/unifsearch/prc_set.php";
    public static final String LINK_SEARCH_SAVE_SETTING = "/jqmobile/view/action/Unifsearch_Action.php";
    public static final String LOG_OUT_ACTION = "logout";
    public static final String RECIVED_TODO = "get";
    public static final String MY_TODO = "my";
    public static final String SEND_TODO = "send";

    public static final int MULTI_SELECT = 1;
    public static final int ITEM_PER_PAGE = 25;
    //GALLERY CONFIG
    public static final int MAX_FILE_SELECTED = 50;
    //File Explorer Constant
    public static final int FILE_TAKE_PHOTO = 1;
    public static final int FILE_TAKE_VIDEO = 2;

    public static final int FILE_CHOOSE_PHOTO = 4;
    public static final int FILE_ETC = 5;
    public static final int ADD_NOTE = 15;
    public static final int LIST_ITEM_COUNT = 15;

    public static final int ID_VIEW_LAYOUT = 1000;
    public static final int ID_DIARY_WRITE_LAYOUT = 1001;
    public static final int ID_WRITE_LAYOUT = 1004;

    public static final int ID_TASK_WRITE_LAYOUT = 1003;
    public static final int ID_TREE_LAYOUT = 1005;
    public static final int FILE_CHOOSE_VIDEO = 6;
    public static final int ACTIVITY_ADD_CONTACT = 7;
    public static final int ID_DELETE_MULTIPLE = 8;
    public static final int ID_DELETE_COMMENT = 124;

    public static final int ID_EDIT_COMMENT = 17;
    public static final int ID_READ_HISTORY = 115;
    public static final int ID_SYNC_DATA = 116;
    public static final int ID_SYNC_SAVE = 117;

    public static final int ID_TAKE_PICTURE = 118;
    public static final int ID_ATTACH_FILE = 119;
    public static final int ID_SETING_AVAR = 120;
    public static final int ID_SETING_SIGN = 121;
    public static final int ID_TIME_ZONE_LIST = 122;
    public static final int ID_TIMEZONE_SAVE = 123;
    public static final int ID_MOVE_FOLDER = 9;
    public static final int ID_MOVE_GROUP = 10;

    public static final int ID_COPY_FOLDER = 15;
    public static final int ID_WRITE_COMMENT = 16;
    public static final int ID_MOVE_SPAM = 10;
    public static final int ID_MULTY_MOVE_SPAM = 11;
    public static final int ID_MULTY_MOVE_FOLDER = 14;

    public static final int ID_FODER_LIST = 125;
    public static final int ID_COPY_FODER_LIST = 126;
    public static final int ID_TARGET_LIST = 128;
    public static final int ID_TO_LIST = 129;
    public static final int ID_CC_LIST = 130;
    public static final int ID_MOVE_CLOUD = 131;
    public static final int ID_ADD_MORE = 132;
    public static final int ID_INIT_DATA = 134;

    public static final int ID_RESIZE_IMAGE = 135;

    public static final int NETWORK_ERROR = 136;
    public static final int ID_WORK_PROCESS = 137;
    public static final int ID_ACTIVITY_REFERENCE = 138;
    public static final int ID_ACTIVITY_MEMO = 139;
    public static final int ID_LIST_REQUEST = 140;
    public static final int ID_REFRESH_LIST = 141;
    public static final int ID_REQUEST_TREE = 142;
    public static final int ID_DELETE_VIEW = 144;

    public static final int ADD_SIGN = 143;
    public static final int ID_CLOUD_ATTACH = 144;
    public static final int ID_REFRESH_TREE = 145;
    public static final int ID_REQUEST_SUBTREE = 146;
    public static final int ID_EDIT_INIT = 147;
    public static final int REQUEST_ID_WRITE = 148;
    public static final int ID_SAVE = 149;
    public static final int FILE_RECORD_AUDIO = 150;
    public static final int ACTIVITY_ADD_ACCOUNT = 151;

    public static final int ID_DATE_PICKER = 153;
    public static final int ID_TIME_PICKER = 155;

    public static final int ID_ACTIVITY_HTML = 156;
    public static final int ID_TEMP_VIEW = 157;
    public static final String MIN_CLOUD_DISK_VERSION = "1.0.2.24";
    public static final int ID_FINISHED = 157;
    public static final int ID_UPDATE = 158;


    public static final int ID_START_UPDATE = 159;
    public static final int ID_SEARCH_LIST = 159;
    public static final int ID_SEARCH_SETTING = 160;
    public static final int ID_TODO_COMPLETE = 161;
    public static final int ID_MAIL_FOLDER = 162;
    public static final int ID_BACK_HOME = 163;
    public static final int ID_REFRESH_VIEW = 164;
    public static final int ID_REQUEST_VIEW = 165;
    public static final int ID_CHECK_PASS = 166;


    public static final int ERROR_INVAILID_INPUT = 167;
    public static final int ERROR_MISSING_FIELD = 168;
    public static final int ID_OPTION1 = 169;
    public static final int ID_OPTION2 = 170;

    public static final String WRITE_MODE_ADD = "add";
    public static final String WRITE_MODE_EDIT = "edit";
    public static final int ID_DELETE_ATTACHMENT = 171;
    public static final int ID_VALIDATE_DATA = 172;
    public static final int ID_INPUT_PASS = 173;
    public static final int ID_INPUT_TEXT = 175;
    public static final int ID_REPLY_COMMENT = 174;
    public static int NOTI_REQUEST_CODE = 28;


    public static final String LINK_GET_DOCUMENT_ID = "/eapproval/?m=json&r=getPushId&id=%s";
    public static final int ID_GET_DOCUMENT_ID = 201;


    public static String FORMAT_ACTIVITY_ACTION = "com.hanbiro.%s";
    public static String FONT_AWARESOME_FILE = "menu_images.ttf";

}
