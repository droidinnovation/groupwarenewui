package com.hanbiro.groupwaredemo.database;

public class NotificationAdapter {
	public static final String TAG = "DatabaseCategoryAdapter";

	public static final String TABLE = "NOTIFICATION";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_TITLE = "TITLE";
	public static final String COLUMN_NEW = "NEW";
	public static final String COLUMN_TYPE = "DATATYPE";
	public static final String COLUMN_SENDER = "SENDER";
	public static final String COLUMN_TIME = "TIME";
	public static final String COLUMN_DOMAIN = "DOMAIN";
	public static final String COLUMN_USER = "USER";
    public static final String COLUMN_DOCU_ID = "A_ID";

	public static final String FECHALL_SELECT = "SELECT " + COLUMN_ID + " ,"
			+ COLUMN_TITLE + ", " + COLUMN_NEW + ", " + COLUMN_TYPE + ", "
			+ COLUMN_SENDER + ", " + COLUMN_DOCU_ID + " FROM " + TABLE;

	public static final String CHECK_EXIST_QUERY = "SELECT 1 from " + TABLE
			+ " where " + COLUMN_ID + " =%s";

//	private Context m_Context;
//	private DataBaseHelper m_DBHelper;
//	private SQLiteDatabase m_Database;
//
//	public NotificationAdapter(Context context) {
//		m_Context = context;
//	}
//
//	public synchronized NotificationAdapter open() {
//		if (m_DBHelper == null) {
//			m_DBHelper = DataBaseHelper.getInstance(m_Context
//					.getApplicationContext());
//		}
//
//		m_Database = m_DBHelper.getWritableDatabase();
//		return this;
//	}
//
//	public int deleteAll() {
//		return m_Database.delete(TABLE, null, null);
//	}
//
//	public void close() {
//		// m_Database.close();
//		// m_DBHelper.close();
//	}
//
//	public boolean exist(ContentValues values) {
//		String userId = values.getAsString(COLUMN_ID);
//		Cursor cursor = m_Database.rawQuery("select 1 from " + TABLE
//				+ " where " + COLUMN_ID + " =?", new String[] { userId });
//		boolean exists = (cursor.getCount() > 0);
//		cursor.close();
//		return exists;
//	}
//
//	public boolean exist(String id) {
//		Cursor cursor = m_Database.rawQuery("select 1 from " + TABLE
//				+ " where " + COLUMN_ID + " =?", new String[] { id });
//		boolean exists = (cursor.getCount() > 0);
//		cursor.close();
//		return exists;
//	}
//
////	public void insert(String title, String strNew, String datetype,
////			String sender, String artical) {
////		ContentValues values = createValue(title, strNew, datetype, sender, artical);
////		m_Database.insert(TABLE, null, values);
////	}
//
//
////	public void insertIdItem(String title, String strNew, String datetype,
////			String sender) {
////		try {
////			ContentValues values = createValue(title, strNew, datetype, sender);
////			m_Database.insert(TABLE, null, values);
////		} catch (android.database.sqlite.SQLiteConstraintException se) {
////			se.printStackTrace();
////		}
////	}
//
//
//    public void insert(ReceivedPushModel model) {
//        ContentValues values = createValue(model.get_title(), model.get_new(),
//                model.get_type(), model.get_sender(), model.get_time(),
//                AppSetting.getInstance(m_Context).getDomain(), AppSetting
//                        .getInstance(m_Context).getUserId(), model.getDocuId());
//
//        m_Database.insert(TABLE, null, values);
//    }
//
//	public long insertIdItem(ContentValues values) {
//		try {
//
//			return m_Database.insert(TABLE, null, values);
//		} catch (android.database.sqlite.SQLiteConstraintException se) {
//			se.printStackTrace();
//		}
//
//		return 0;
//	}
//
//
//	public ListHistoryPush fetchAllHistoryPush() {
//		ListHistoryPush listPush = new ListHistoryPush();
//
//		Cursor cursor = m_Database.query(TABLE, new String[] { COLUMN_ID,
//				COLUMN_TITLE, COLUMN_NEW, COLUMN_TYPE, COLUMN_SENDER }, null,
//				null, null, null, null);
//
//		if (cursor != null) {
//			if (cursor.moveToFirst()) {
//				int colID = cursor.getColumnIndex(COLUMN_ID);
//				int colTitle = cursor.getColumnIndex(COLUMN_TITLE);
//				int colNew = cursor.getColumnIndex(COLUMN_NEW);
//				int colTime = cursor.getColumnIndex(COLUMN_TYPE);
//				int colSender = cursor.getColumnIndex(COLUMN_SENDER);
//				do {
//					ReceivedPushModel modelPush = new ReceivedPushModel();
//					modelPush.set_id(cursor.getInt(colID));
//					modelPush.set_title(cursor.getString(colTitle));
//					modelPush.set_new(cursor.getString(colNew));
//					modelPush.set_time(cursor.getLong(colTime));
//					modelPush.set_sender(cursor.getString(colSender));
//
//					listPush.add(modelPush);
//				} while (cursor.moveToNext());
//			}
//			cursor.close();
//		}
//		return listPush;
//	}
//
//	public ListHistoryPush fetchHistoryPushOffset(int offset, int length) {
//		ListHistoryPush listPush = new ListHistoryPush();
//
//		Cursor cursor = m_Database.rawQuery("SELECT * FROM " + TABLE
//				+ " LIMIT " + length + " OFFSET " + offset, null);
//
//		if (cursor != null) {
//			if (cursor.moveToFirst()) {
//				int colID = cursor.getColumnIndex(COLUMN_ID);
//				int colTitle = cursor.getColumnIndex(COLUMN_TITLE);
//				int colNew = cursor.getColumnIndex(COLUMN_NEW);
//				int colType = cursor.getColumnIndex(COLUMN_TYPE);
//				int colSender = cursor.getColumnIndex(COLUMN_SENDER);
//				int colTime = cursor.getColumnIndex(COLUMN_TIME);
//				do {
//					ReceivedPushModel modelPush = new ReceivedPushModel();
//					modelPush.set_id(cursor.getInt(colID));
//					modelPush.set_title(cursor.getString(colTitle));
//					modelPush.set_new(cursor.getString(colNew));
//					modelPush.set_time(cursor.getLong(colTime));
//					modelPush.set_sender(cursor.getString(colSender));
//					modelPush.set_type(cursor.getString(colType));
//					listPush.add(modelPush);
//				} while (cursor.moveToNext());
//			}
//			cursor.close();
//		}
//		return listPush;
//	}
//
//	public ListHistoryPush fetchHistoryPushOffsetByAccount(int offset,
//			int length) {
//		ListHistoryPush listPush = new ListHistoryPush();
//
////		String sqlTest = "SELECT * FROM " + TABLE + " WHERE " + COLUMN_DOMAIN
////				+ "='" + AppSetting.getInstance(m_Context).getDomain() + "'"
////				+ " AND " + COLUMN_USER + "='"
////				+ AppSetting.getInstance(m_Context).getUserId() + "'"
////				+ " ORDER BY " + COLUMN_TIME + " DESC" + " LIMIT " + length
////				+ " OFFSET " + offset;
////		Log.v(sqlTest);
//
//		Cursor cursor = m_Database.rawQuery(
//				"SELECT * FROM " + TABLE + " WHERE " + COLUMN_DOMAIN + "='"
//						+ AppSetting.getInstance(m_Context).getDomain() + "'"
//						+ " AND " + COLUMN_USER + "='"
//						+ AppSetting.getInstance(m_Context).getUserId() + "'"
//						+ " ORDER BY " + COLUMN_TIME + " DESC"
//						+ " LIMIT " + length + " OFFSET " + offset, null);
//
//		if (cursor != null) {
//			if (cursor.moveToFirst()) {
//				int colID = cursor.getColumnIndex(COLUMN_ID);
//				int colTitle = cursor.getColumnIndex(COLUMN_TITLE);
//				int colNew = cursor.getColumnIndex(COLUMN_NEW);
//				int colType = cursor.getColumnIndex(COLUMN_TYPE);
//				int colSender = cursor.getColumnIndex(COLUMN_SENDER);
//				int colTime = cursor.getColumnIndex(COLUMN_TIME);
//                int colDocId = cursor.getColumnIndex(COLUMN_DOCU_ID);
//				do {
//					ReceivedPushModel modelPush = new ReceivedPushModel();
//					modelPush.set_id(cursor.getInt(colID));
//					modelPush.set_title(cursor.getString(colTitle));
//					modelPush.set_new(cursor.getString(colNew));
//					modelPush.set_time(cursor.getLong(colTime));
//					modelPush.set_sender(cursor.getString(colSender));
//					modelPush.set_type(cursor.getString(colType));
//                    modelPush.setDocuId(cursor.getString(colDocId));
//					listPush.add(modelPush);
//				} while (cursor.moveToNext());
//			}
//			cursor.close();
//		}
//		return listPush;
//	}
//
//	public ListHistoryPush fetchHistoryPushOffsetByAccountAndCategory(int offset, int length, String type) {
//		ListHistoryPush listPush = new ListHistoryPush();
//
////		String sqlTest = "SELECT * FROM " + TABLE + " WHERE " + COLUMN_DOMAIN
////				+ "='" + AppSetting.getInstance(m_Context).getDomain() + "'"
////				+ " AND " + COLUMN_USER + "='"
////				+ AppSetting.getInstance(m_Context).getUserId() + "'" + " AND " + COLUMN_TYPE + "='" + type + "'"
////				+ " ORDER BY " + COLUMN_TIME + " DESC" + " LIMIT " + length
////				+ " OFFSET " + offset;
////		Log.v(sqlTest);
//
//		Cursor cursor = m_Database.rawQuery(
//				"SELECT * FROM " + TABLE + " WHERE " + COLUMN_DOMAIN + "='"
//						+ AppSetting.getInstance(m_Context).getDomain() + "'"
//						+ " AND " + COLUMN_USER + "='"
//						+ AppSetting.getInstance(m_Context).getUserId() + "'"
//                        + " AND " + COLUMN_TYPE + "='" + type + "'"
//						+ " ORDER BY " + COLUMN_TIME + " DESC"
//						+ " LIMIT " + length + " OFFSET " + offset, null);
//
//		if (cursor != null) {
//			if (cursor.moveToFirst()) {
//				int colID = cursor.getColumnIndex(COLUMN_ID);
//				int colTitle = cursor.getColumnIndex(COLUMN_TITLE);
//				int colNew = cursor.getColumnIndex(COLUMN_NEW);
//				int colType = cursor.getColumnIndex(COLUMN_TYPE);
//				int colSender = cursor.getColumnIndex(COLUMN_SENDER);
//				int colTime = cursor.getColumnIndex(COLUMN_TIME);
//				do {
//					ReceivedPushModel modelPush = new ReceivedPushModel();
//					modelPush.set_id(cursor.getInt(colID));
//					modelPush.set_title(cursor.getString(colTitle));
//					modelPush.set_new(cursor.getString(colNew));
//					modelPush.set_time(cursor.getLong(colTime));
//					modelPush.set_sender(cursor.getString(colSender));
//					modelPush.set_type(cursor.getString(colType));
//					listPush.add(modelPush);
//				} while (cursor.moveToNext());
//			}
//			cursor.close();
//		}
//		return listPush;
//	}
//
//
//	public ListTypePush fetchTypePush() {
//		ListTypePush listTypePush = new ListTypePush();
//
//		Cursor cursor = m_Database.rawQuery("SELECT DISTINCT "
//                + COLUMN_TYPE + " COLLATE NOCASE FROM " + TABLE, null);
//
//		if (cursor != null) {
////            int colType = cursor.getColumnIndex(COLUMN_TYPE);
//
//            if (cursor.moveToFirst()) {
//				do {
//					TypePushModel modelType = new TypePushModel();
//					modelType.setType(cursor.getString(0));
//					modelType.setDisplay(NotificationUtil.stylePush(m_Context,cursor.getString(0)));
//					listTypePush.add(modelType);
//				} while (cursor.moveToNext());
//			}
//			cursor.close();
//		}
//		return listTypePush;
//	}
//
//
//	public Cursor fetchId(long rowID) {
//
//		Cursor cursor = m_Database.query(true, TABLE, new String[] { COLUMN_ID,
//				COLUMN_TITLE, COLUMN_NEW, COLUMN_TYPE, COLUMN_SENDER },
//				COLUMN_ID + " = " + rowID, null, null, null, null, null);
//		if (cursor != null) {
//			cursor.moveToFirst();
//		}
//
//		return cursor;
//	}
//
//	public boolean deleteId(long rowId) {
//		return m_Database.delete(TABLE, COLUMN_ID + "=" + rowId, null) > 0;
//	}
//
////	public static ContentValues createValue(String title, String strNew,
////			String datetype, String sender) {
////		ContentValues value = new ContentValues();
////		value.put(COLUMN_TITLE, title);
////		value.put(COLUMN_NEW, strNew);
////		value.put(COLUMN_TYPE, datetype);
////		value.put(COLUMN_SENDER, sender);
////		return value;
////	}
//
//	public static ContentValues createValue(String title, String strNew,
//			String datetype, String sender, long time, String domain,
//			String user, String docuId) {
//		ContentValues value = new ContentValues();
//		value.put(COLUMN_TITLE, title);
//		value.put(COLUMN_NEW, strNew);
//		value.put(COLUMN_TYPE, datetype);
//		value.put(COLUMN_SENDER, sender);
//		value.put(COLUMN_TIME, time);
//		value.put(COLUMN_DOMAIN, domain);
//		value.put(COLUMN_USER, user);
//        value.put(COLUMN_DOCU_ID, docuId);
//		return value;
//	}

}
