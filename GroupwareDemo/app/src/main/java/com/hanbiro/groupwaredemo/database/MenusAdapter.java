package com.hanbiro.groupwaredemo.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class MenusAdapter {
	public static final String TAG = "DatabaseCategoryAdapter";
	
	public static final String TABLE = "CATEGORY";
	public static final String KEY_ROWID = "_id";
	public static final String KEY_NAME = "name";
	public static final String KEY_LABEL = "label";
	public static final String KEY_STATE_0 = "state0";
	public static final String KEY_STATE_1 = "state1";
	
	
	private Context m_Context;
	private DataBaseHelper m_DBHelper;
	private SQLiteDatabase m_Database;
	
	public MenusAdapter(Context context) {
		m_Context = context;
	}
	
	public synchronized MenusAdapter open(){
		if(m_DBHelper == null){
			m_DBHelper = DataBaseHelper.getInstance(m_Context.getApplicationContext());
		}
		
		m_Database = m_DBHelper.getWritableDatabase();
		return this;
	}
	
	public void deleteAll()
	{
		m_Database.delete(TABLE, null, null);

	}
	
	public void close(){
//		m_Database.close();
//		m_DBHelper.close();
	}
	
	public void insert(ContentValues values){
		m_Database.insert(TABLE, null, values);
	}
	
	public void insertCategory(String name, String label, int[] state){
		
		ContentValues values = createCategoryValue(name, label, state[0], state[1]);
		m_Database.insert(TABLE, null, values);
	}
	
	
	public Cursor fetchAll() {		
		Cursor cursor = m_Database.query(TABLE, new String[] {KEY_ROWID, KEY_NAME,
				KEY_LABEL, KEY_STATE_0, KEY_STATE_1}, null, null, null, null, null);
		cursor.moveToFirst();
		return cursor;
	}
	
	public Cursor fetchFavorite(long rowID) {
		
		Cursor cursor = m_Database.query(true, TABLE, new String[] { KEY_NAME,
				KEY_LABEL, KEY_STATE_0, KEY_STATE_1}, KEY_ROWID + " = " + rowID, null, null, null, null, null);		
		if(cursor != null){
			cursor.moveToFirst();
		}
		
		return cursor;
	}
	
	public boolean deleteCategory(long rowId) {
		
		return m_Database.delete(TABLE, KEY_ROWID + "=" + rowId, null) > 0;
	}

	private ContentValues createCategoryValue(String name, String label, int state0, int state1){
		ContentValues value = new ContentValues();
		value.put(KEY_NAME, name);
		value.put(KEY_LABEL, label);
		value.put(KEY_STATE_0, state0);	
		value.put(KEY_STATE_1, state1);	
		return value;
	}
}
