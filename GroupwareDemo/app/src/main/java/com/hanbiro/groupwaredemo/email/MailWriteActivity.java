package com.hanbiro.groupwaredemo.email;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.hanbiro.groupwaredemo.R;
import com.hanbiro.groupwaredemo.base.BaseActivity;

/**
 * Created by NguyenQuang on 3/18/15.
 */
public class MailWriteActivity extends BaseActivity {

    private static final String FRAGMENT_WRITE_MAIL_TAG = "fragment_write_mail_tag";
    private MailWriteFragment mailWriteFragment;
    private static final String TAG = "Compose Mail";

    private CharSequence mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_writemail);

        //action bar setup
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolBar);
        if (toolbar!=null){
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }


        mTitle = getBaseTitle();

        if (savedInstanceState == null) {
            mailWriteFragment = MailWriteFragment.createInstance(null);
            getSupportFragmentManager().beginTransaction().add(R.id.frame_write_mail, mailWriteFragment, FRAGMENT_WRITE_MAIL_TAG).commit();
        } else {
            mailWriteFragment = (MailWriteFragment) getSupportFragmentManager().findFragmentByTag(FRAGMENT_WRITE_MAIL_TAG);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_write_mail, menu);
        restoreActionBar();
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
       /* if (id == R.id.action_settings) {
            return true;
        }*/
        return super.onOptionsItemSelected(item);
    }


    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }

    public static void startMailWriteActivity(Context context) {
        Intent intent = new Intent(context, MailWriteActivity.class);
        context.startActivity(intent);
    }
    protected String getBaseTitle(){
        return TAG;
    }
}
