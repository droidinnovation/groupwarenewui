package com.hanbiro.groupwaredemo.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class OSSetting {
	private static List<String> languageList = new ArrayList<String>();
	static {
		languageList.add("ko");
		languageList.add("ja");
		languageList.add("zh");
		languageList.add("en");
		languageList.add("es");
		languageList.add("de");
		languageList.add("vi");	
	}
	
	public static String getLanguageCode(){
		String phoneLanguage = Locale.getDefault().getLanguage();
		if(languageList != null && languageList.contains(phoneLanguage))
			return phoneLanguage;
		else
			return "en";
	}
}
