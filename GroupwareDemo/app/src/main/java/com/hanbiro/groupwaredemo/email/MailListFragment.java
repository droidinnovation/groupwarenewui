package com.hanbiro.groupwaredemo.email;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.hanbiro.groupwaredemo.C;
import com.hanbiro.groupwaredemo.R;
import com.hanbiro.groupwaredemo.base.ProgressableFragment;
import com.hanbiro.groupwaredemo.email.models.MailBoxItemModel;
import com.hanbiro.groupwaredemo.email.models.MailBoxModel;
import com.hanbiro.groupwaredemo.email.models.MailListModel;
import com.hanbiro.groupwaredemo.email.views.MailBoxAdapter;
import com.hanbiro.groupwaredemo.email.views.MailListAdapter;

/**
 * Created by MinhNguyen on 3/17/15.
 */
public class MailListFragment extends ProgressableFragment implements SwipeRefreshLayout.OnRefreshListener{

    public static MailListFragment createInstance(MailBoxItemModel item){
        MailListFragment fragment = new MailListFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(C.KEY_DATA, item);
        fragment.setArguments(bundle);
        return fragment;
    }
    private static final String TAG = "maillist";
    private RecyclerView listView;
    private SwipeRefreshLayout swLayout;
    private RequestManager requestManager;
    private RecyclerView.LayoutManager mLayoutManager;
    private MailListModel model;
    private MailListAdapter adapter;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        requestManager = new RequestManager(activity, this);
        setRetainInstance(true);
    }

    @Override
    public View onCreateMyView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.mail_list_layout, null);
        swLayout = (SwipeRefreshLayout) view.findViewById(R.id.swLayout);
        mLayoutManager = new LinearLayoutManager(getActivity());
        listView = (RecyclerView) view.findViewById(R.id.listView);
        listView.setLayoutManager(mLayoutManager);
        swLayout.setOnRefreshListener(this);

        view.findViewById(R.id.buttonFloat).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MailWriteActivity.startMailWriteActivity(getActivity());
            }
        });

        view.findViewById(R.id.btBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(model == null)
            sendRequest(true, true, 0);
        else
            setModel(model);

    }

    private void setModel(MailListModel model) {
        this.model = model;
        adapter = new MailListAdapter(model.getMaillist(), getActivity());
        listView.setAdapter(adapter);
        setViewShow(true);
    }

    private void sendRequest(boolean cache, boolean progress, int start){
        setViewShow(!progress);
        MailBoxItemModel boxItemModel = getArguments().getParcelable(C.KEY_DATA);
        Request request = requestManager.mailList(listener, boxItemModel.getKey(), start);
        request.setShouldCache(cache);
        request.setTag(TAG);
        sendRequest(request, progress);
    }

    private boolean addMore = false;

    private Response.Listener<MailListModel> listener = new Response.Listener<MailListModel>() {
        @Override
        public void onResponse(MailListModel response) {
            if(response != null){
                setModel(response);
                swLayout.setRefreshing(false);
            }

        }
    };

    @Override
    public void onRefresh() {
        sendRequest(false, false, 0);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getRequestPool().getRequestQueue().cancelAll(TAG);
    }
}
