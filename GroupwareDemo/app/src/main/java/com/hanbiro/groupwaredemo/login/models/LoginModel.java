package com.hanbiro.groupwaredemo.login.models;




import com.hanbiro.groupwaredemo.C;
import com.hanbiro.groupwaredemo.R;
import com.hanbiro.groupwaredemo.login.AddAccounter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

public class LoginModel {

    public static interface OnHomeDataChanged {
        public void onHomeDataChanged(List<HomeItemModel> data);
    }


    private int ok;
    private String msg;
    private String title;
    private String domain;
    private List<MenuItemModel> menu;
    private String cookie;
    private String lang;
    private String hmail_key;
    private List<HomeItemModel> data;
    private boolean today;
    private String zone_name;
    private String dateform;
    private String timezone;
    private String u_cn;
    private String u_no;
    private String date;
    private String time;
    private boolean cloud;

    private List<OnHomeDataChanged> listener = new ArrayList<OnHomeDataChanged>(0);

    public boolean isCloud() {
        return cloud;
    }

    public void setCloud(boolean cloud) {
        this.cloud = cloud;
    }

    public void addHomeChangedListener(OnHomeDataChanged dataChanged) {
        if (listener != null)
            listener.add(dataChanged);
    }

    public void removeHomeChangedListener(OnHomeDataChanged changed) {
        if (listener != null)
            listener.remove(changed);
    }

    private void notifyHomeChanged() {
        validateHomeData();

        for (OnHomeDataChanged item : listener) {
            item.onHomeDataChanged(data);
        }
    }

    public void reset() {
        title = "";
        domain = "";
        cookie = "";
        lang = "";
        hmail_key = "";
        zone_name = "";
        date = "";
        time = "";
        timezone = "";
        u_cn = "";
        u_no = "";
        menu = null;
        data = null;
    }


    public void setCokieFromToken(String token) {
        StringTokenizer tokenizer = new StringTokenizer(token, AddAccounter.TOKEN_SEPARATER);
        if (tokenizer.hasMoreTokens()) {
            cookie = tokenizer.nextToken();
        }
        if (tokenizer.hasMoreTokens()) {
            hmail_key = tokenizer.nextToken();
        }
    }


    public void setDateForm(String dateform) {
        StringTokenizer tokenizer = new StringTokenizer(dateform, " ");

        if (tokenizer.hasMoreElements())
            date = tokenizer.nextToken();
        if (tokenizer.hasMoreElements())
            time = tokenizer.nextToken();

    }

    public void init() {
        if ("null".equals(dateform) || dateform == null)
            dateform = "";
        else {

            dateform = dateform.replace("Y", "yyyy");
            dateform = dateform.replace("m", "MM");
            dateform = dateform.replace("d", "dd");
            dateform = dateform.replace("H", "HH");
            dateform = dateform.replace("i", "mm");
            dateform = dateform.replace(":s", "");
        }

        setDateForm(dateform);
        IconMap iconMap = new IconMap();

        for (MenuItemModel item : menu) {
            int icon = iconMap.getIconText(item.getName());
            if(icon != 0){
                item.setImage(icon);
            }
        }

        //        Todo : organization debug
//        MenuItemModel organization = new MenuItemModel();
//        organization.setName(C.MENU_ORGANIZATION);
//        organization.setOffIcon(R.drawable.icon_organization);
//        organization.setOnIcon(R.drawable.icon_organization_on);
//        organization.setTitle("organization");
//
//        if (BuildConfig.DEBUG)
//            menu.add(0, organization);

        validateHomeData();


    }

    private void validateHomeData() {
        if (data != null) {
            Iterator<HomeItemModel> iterator = data.iterator();
            while (iterator.hasNext()) {
                HomeItemModel homeItemModel = (HomeItemModel) iterator.next();
                if (C.getInstance().ABSENT_LIST.contains(homeItemModel.getName())) {
                    iterator.remove();
                }
            }
        }
    }


    public String getDateform() {
        return dateform;
    }


    public String getDate() {
        return date;
    }


    public void setDate(String date) {
        this.date = date;
    }


    public String getTime() {
        return time;
    }


    public void setTime(String time) {
        this.time = time;
    }


    public int getOk() {
        return ok;
    }

    public void setOk(int ok) {
        this.ok = ok;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEmailDomain() {
        return domain;
    }

    public void setEmailDomain(String domain) {
        this.domain = domain;
    }

    public List<MenuItemModel> getMenu() {
        return menu;
    }


    public void setMenu(List<MenuItemModel> menu) {
        this.menu = menu;
    }


    public String getCookie() {
        return cookie;
    }

    public void setCookie(String cookie) {
        this.cookie = cookie;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getHmail_key() {
        return hmail_key;
    }

    public void setHmail_key(String hmail_key) {
        this.hmail_key = hmail_key;
    }

    public List<HomeItemModel> getData() {
        return data;
    }

    public void setData(List<HomeItemModel> data) {
        this.data = data;
        notifyHomeChanged();
    }

    public boolean isToday() {
        return today;
    }

    public void setToday(boolean today) {
        this.today = today;
    }

    public String getZone_name() {
        return zone_name;
    }

    public void setZone_name(String zone_name) {
        this.zone_name = zone_name;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getU_cn() {
        return u_cn;
    }

    public void setU_cn(String u_cn) {
        this.u_cn = u_cn;
    }

    public String getU_no() {
        return u_no;
    }

    public void setU_no(String u_no) {
        this.u_no = u_no;
    }


    public String getMsg() {
        return msg;
    }


    public void setMsg(String msg) {
        this.msg = msg;
    }


}
