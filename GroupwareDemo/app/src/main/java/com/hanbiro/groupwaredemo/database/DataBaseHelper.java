package com.hanbiro.groupwaredemo.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DataBaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "HanBiroGroupware";
    private static final int VERSION_12 = 12;
    private static final int VERSION_11 = 11;
    private static final int VERSION_10 = 10;
    private static final int VERSION_13 = 13;
    private static final int VERSION_14 = 14;
    private static final int VERSION_15 = 15;
    private static final int VERSION_16 = 16;
    private static final int VERSION_17 = 17;
    private static final int VERSION_18 = 18;
    private static final int VERSION_19 = 19;
    private static final int DATABASE_VERSION = VERSION_19;

    private static DataBaseHelper instance;

    private static final String CATEGORY_CREATE_STATEMENT = "CREATE TABLE "
            + MenusAdapter.TABLE + " (_id integer primary key autoincrement,"
            + MenusAdapter.KEY_NAME + " text not null, "
            + MenusAdapter.KEY_LABEL + " text not null, "
            + MenusAdapter.KEY_STATE_0 + " INTEGER not null, "
            + MenusAdapter.KEY_STATE_1 + " INTEGER not null)";

    private static final String HOMEITEMS_CREATE_STATEMENT = "CREATE TABLE "
            + HomeItemsAdapter.TABLE
            + " (_id integer primary key autoincrement,"
            + HomeItemsAdapter.KEY_NAME + " text not null, "
            + HomeItemsAdapter.KEY_NUM + " INTEGER not null, "
            + HomeItemsAdapter.KEY_TITLE + " text not null, "
            + HomeItemsAdapter.KEY_SHOW + " INTEGER not null)";

    // domain table
    private static final String DOMAINS_CREATE_STATEMENT = "CREATE TABLE "
            + DBDomainAdapter.TABLE
            + " (_id integer primary key autoincrement,"
            + DBDomainAdapter.KEY_DOMAIN_NAME + " text unique)";

    // user id table
    private static final String ID_CREATE_STATEMENT = "CREATE TABLE "
            + DBIdAdapter.TABLE + " (_id integer primary key autoincrement,"
            + DBIdAdapter.KEY_NAME + " text unique)";

    private static final String ACCOUNT_CREATE_STATEMENT = "CREATE TABLE "
            + AccountAdapter.TABLE + " (_id integer primary key autoincrement,"
            + AccountAdapter.COLUMN_USER_ID + " text, "
            + AccountAdapter.COLUMN_DOMAIN + " text)";

    // user id table
    public static final String NOTIFICATION_CREATE_STATEMENT = "CREATE TABLE "
            + NotificationAdapter.TABLE
            + " (_id integer primary key autoincrement, "
            + NotificationAdapter.COLUMN_TYPE + " text, "
            + NotificationAdapter.COLUMN_TIME + " text, "
            + NotificationAdapter.COLUMN_NEW + " text, "
            + NotificationAdapter.COLUMN_SENDER + " text, "
            + NotificationAdapter.COLUMN_DOMAIN + " text, "
            + NotificationAdapter.COLUMN_USER + " text, "
            + NotificationAdapter.COLUMN_DOCU_ID + " text, "
            + NotificationAdapter.COLUMN_TITLE + " text)";

    // emails
    private static final String MAIL_LIST_CREATE_STATEMENT = "CREATE TABLE "
            + MailItemsAdapter.TABLE + " (" + MailItemsAdapter.COLUMN__ROWID
            + " integer primary key autoincrement,"
            + MailItemsAdapter.COLUMN_EMAIL + " text)";

    public synchronized static DataBaseHelper getInstance(Context context) {
        if (instance == null)
            instance = new DataBaseHelper(context);

        return instance;
    }

    private DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION_19);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CATEGORY_CREATE_STATEMENT);
        db.execSQL(HOMEITEMS_CREATE_STATEMENT);
        db.execSQL(DOMAINS_CREATE_STATEMENT);
        db.execSQL(ID_CREATE_STATEMENT);
        db.execSQL(ACCOUNT_CREATE_STATEMENT);
        db.execSQL(NOTIFICATION_CREATE_STATEMENT);
        db.execSQL(MAIL_LIST_CREATE_STATEMENT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(DataBaseHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");


        switch (oldVersion) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case VERSION_10:
            case VERSION_11:
            case VERSION_12:
            case VERSION_13:
            case VERSION_14:
            case VERSION_15:
            case VERSION_16:
            case VERSION_17:
            case VERSION_18:{
                db.execSQL("DROP TABLE IF EXISTS " + MenusAdapter.TABLE);
                db.execSQL("DROP TABLE IF EXISTS " + HomeItemsAdapter.TABLE);
                db.execSQL("DROP TABLE IF EXISTS " + DBDomainAdapter.TABLE);
                db.execSQL("DROP TABLE IF EXISTS " + DBIdAdapter.TABLE);
                db.execSQL("DROP TABLE IF EXISTS " + AccountAdapter.TABLE);
                db.execSQL("DROP TABLE IF EXISTS " + NotificationAdapter.TABLE);
                db.execSQL("DROP TABLE IF EXISTS " + MailItemsAdapter.TABLE);
                onCreate(db);
                break;
            }
            case VERSION_19: {
                break;
            }

            default:
                break;
        }

    }

}
