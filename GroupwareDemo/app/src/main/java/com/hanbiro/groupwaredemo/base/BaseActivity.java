package com.hanbiro.groupwaredemo.base;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.widget.DrawerLayout;

import com.hanbiro.groupwaredemo.AppSetting;
import com.hanbiro.groupwaredemo.C;
import com.hanbiro.groupwaredemo.R;


public abstract class BaseActivity extends ActionBarActivity {

}
