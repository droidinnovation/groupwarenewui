package com.hanbiro.groupwaredemo.login.models;

import android.graphics.Bitmap;
import android.support.v4.util.LruCache;

import com.hanbiro.groupwaredemo.C;
import com.hanbiro.groupwaredemo.R;

import java.util.HashMap;
import java.util.Map;

public class IconMap {



    Map<String, Integer> icons = new HashMap<String, Integer>(20);



//    public static Bitmap getIcon(Context context, int id){
//        if(bitmaps == null){
//            final int memClass = ((ActivityManager) context.getSystemService(
//                    Context.ACTIVITY_SERVICE)).getMemoryClass();
//
//            // Use 1/8th of the available memory for this memory cache.
//            final int cacheSize = 1024 * 1024 * memClass / 8;
//            bitmaps = new LruCache<>(cacheSize);
//        }
//
//        Bitmap bitmap =  bitmaps.get(id);
//        if(bitmap == null){
//            int defaultWidth = context.getResources().getDimensionPixelOffset(R.dimen.bottom_menu_default_width);
//            int defaultHeight = context.getResources().getDimensionPixelOffset(R.dimen.bottom_menu_default_height);
//            bitmap = BitmapUtils.getBitmap(context.getResources(), id, defaultWidth, defaultHeight);
//            bitmaps.put(id, bitmap);
//        }
//        return bitmap;
//    }



	public IconMap() {

		icons.put(C.MENU_APPROVAL, R.string.icon_approval);
		icons.put(C.MENU_CONTACT, R.string.icon_contact);
		icons.put(C.MENU_ALARM, R.string.icon_contact);
		icons.put(C.MENU_BOARD, R.string.icon_board);
		icons.put(C.MENU_CALENDAR, R.string.icon_calendar);
		icons.put(C.MENU_CIRCULAR, R.string.icon_circular);
		icons.put(C.MENU_EQUIP, R.string.icon_circular);
		icons.put(C.MENU_HOLIDAY, R.string.icon_holiday);
		icons.put(C.MENU_HOLIDAY2, R.string.icon_holiday);
		icons.put(C.MENU_LOGOUT, R.string.icon_holiday);
		icons.put(C.MENU_MAIL, R.string.icon_mail);
		icons.put(C.MENU_PC, R.string.icon_pc);
		icons.put(C.MENU_PROJECT, R.string.icon_project);
		icons.put(C.MENU_SEARCH, R.string.icon_project);
		icons.put(C.MENU_TASK1, R.string.icon_task);
		icons.put(C.MENU_TASK2, R.string.icon_task);
		icons.put(C.MENU_TIMECARD, R.string.icon_time_card);
		icons.put(C.MENU_TODO, R.string.icon_todo);
		icons.put(C.MENU_WHISPER, R.string.icon_task);
        icons.put(C.MENU_ARCHIVES, R.string.icon_archives);
        icons.put(C.MENU_ORGANIZATION, R.string.icon_organization);
	}




	public int getIconText(String key){
		return icons.get(key);
	}
}
