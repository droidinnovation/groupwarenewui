package com.hanbiro.groupwaredemo.utils;

import com.hanbiro.groupwaredemo.BuildConfig;

import org.apache.http.NameValuePair;

import java.util.Iterator;
import java.util.List;


public class Log
{
	public final static String LOGTAG = "HANBIRO_GROUPWARE";
    public static final boolean DEBUG = BuildConfig.DEBUG;


	public static void showRequestData(List<NameValuePair> data){
		if(Log.DEBUG)
			if(data != null){
				StringBuffer buffer = new StringBuffer();
				Iterator<NameValuePair> iterator = data.iterator();
				while (iterator.hasNext()) {
					NameValuePair nameValuePair = (NameValuePair) iterator.next();
					buffer.append(nameValuePair.getName() + "=" + nameValuePair.getValue() + "&");
				}
				android.util.Log.v(LOGTAG, buffer.toString());	

			}
	}

	public static void v(String msg) 
	{
		android.util.Log.v(LOGTAG, msg);
	}

	public static void v(String tag, String msg) 
	{
		android.util.Log.v(LOGTAG+" "+tag, msg);
	}

	public static void e(String msg) 
	{
		android.util.Log.e(LOGTAG, msg);
	}


	public static void longInfo(String str) {

		while (str.length()  > 4000) {
			Log.v(str.substring(0, 4000));
			str = str.substring(4000);
		}
		Log.v(LOGTAG, str);



	}
}
