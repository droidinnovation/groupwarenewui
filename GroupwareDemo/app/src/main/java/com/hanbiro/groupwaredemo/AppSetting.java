package com.hanbiro.groupwaredemo;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;

import com.hanbiro.groupwaredemo.database.HomeItemsAdapter;
import com.hanbiro.groupwaredemo.database.MenusAdapter;
import com.hanbiro.groupwaredemo.login.models.HomeItemModel;
import com.hanbiro.groupwaredemo.login.models.HomeModel;
import com.hanbiro.groupwaredemo.login.models.LoginModel;
import com.hanbiro.groupwaredemo.login.models.MenuItemModel;
import com.hanbiro.groupwaredemo.utils.Log;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class AppSetting {

	public static final String SHARED_PREFS_FILE = "AppSetting";
	private static final String SHARED_PREFS_COOKIE_KEY = "strCookie";
	private static final String SHARED_PREFS_HMAILKEY_KEY = "strHmailKey";
	private static final String SHARED_PREFS_APP_TITLE_KEY = "apptitle";
	private static final String SHARED_PREFS_USER_NAME_KEY = "username";
	private static final String SHARED_PREFS_USER_PASS_KEY = "userpass";
	private static final String SHARED_PREFS_DATEFORM_KEY = "dateform";
	private static final String SHARED_PREFS_DOMAIN_KEY = "strDomain";
	private static final String SHARED_PREFS_MAIL_DOMAIN = "mail_domain";
	private static final String SHARED_PREFS_LOGIN = "login";
	private static final String SHARED_PREFS_NOTE_WIDTH = "w";
	private static final String SHARED_PREFS_NOTE_HEIGHT = "h";
	private static final String SHARED_PREFS_USER_CN_KEY = "cn";
	private static final String SHARED_PREFS_USER_NO_KEY = "no";
	private static final String SHARED_PREFS_ZONE_KEY = "zone";
	private static final String SHARED_PREFS_ZONE_VALUE = "tz";
    private static final String SHARED_PREFS_CLOUD_VALUE = "cl";

	
	private static final String TAG = "HanbiroAppSetting";


	private String  userName;
	private String password;

	private int noteWidth;
	private int noteHeight;
	private String domain;
	private boolean logIn;
	private LoginModel loginModel;
	private static AppSetting instance;

	public static AppSetting getInstance(Context context){
		if(instance == null){
			instance = new AppSetting(context);
		}

		return instance;
	}


	private AppSetting(Context context){

		if(Log.DEBUG)
			Log.v(TAG, "HanbiroAppSetting read Data");

		SharedPreferences prefs = context.getSharedPreferences(SHARED_PREFS_FILE, Context.MODE_PRIVATE);

		this.loginModel = new LoginModel();

		getMenus(context);
		getHomeItems(context);

		this.loginModel.setTitle(prefs.getString(SHARED_PREFS_APP_TITLE_KEY, ""));
		this.loginModel.setDateForm(prefs.getString(SHARED_PREFS_DATEFORM_KEY, ""));
		this.loginModel.setEmailDomain(prefs.getString(SHARED_PREFS_MAIL_DOMAIN, ""));
		this.loginModel.setU_cn(prefs.getString(SHARED_PREFS_USER_CN_KEY, ""));
		this.loginModel.setU_no(prefs.getString(SHARED_PREFS_USER_NO_KEY, ""));
		this.loginModel.setZone_name(prefs.getString(SHARED_PREFS_ZONE_KEY, ""));
		this.loginModel.setTimezone(prefs.getString(SHARED_PREFS_ZONE_VALUE, ""));

		domain = (prefs.getString(SHARED_PREFS_DOMAIN_KEY, ""));

		this.noteHeight = prefs.getInt(SHARED_PREFS_NOTE_HEIGHT, 0);
		this.noteWidth = prefs.getInt(SHARED_PREFS_NOTE_WIDTH, 0);
		this.logIn = prefs.getBoolean(SHARED_PREFS_LOGIN,false);
		this.userName = prefs.getString(SHARED_PREFS_USER_NAME_KEY, "");
		this.loginModel.setCookie(prefs.getString(SHARED_PREFS_COOKIE_KEY, ""));
		this.loginModel.setHmail_key(prefs.getString(SHARED_PREFS_HMAILKEY_KEY, ""));
        this.loginModel.setCloud(prefs.getBoolean(SHARED_PREFS_CLOUD_VALUE, false));

		if(userName != null && userName.length() > 0){
//			String accountType = context.getString(R.string.ACCOUNT_TYPE);
//            String accountDomain = userName + getMailDomain();
//			Account account = new Account(accountDomain,accountType);
//			AccountManager am = AccountManager.get(context.getApplicationContext());
//			this.password = am.getPassword(account);
//			if(password == null)
//				this.password = prefs.getString(SHARED_PREFS_USER_PASS_KEY, "");
		}

	}

	
	public void logout(Context context){
		setLogIn(false);
		loginModel.reset();
		perSistenceData(context);
	}

	public void reset(Context context){
		if(loginModel != null){
			loginModel.reset();
		}
		userName = null;
		password = null;
		domain = null;
		perSistenceData(context);

	}
	

	
	public void init(LoginModel loginModel){
		this.loginModel = loginModel; 
	}

	public void perSistenceData(Context context)  {

		if(Log.DEBUG)
			Log.v(TAG, "perSistenceData");
		SharedPreferences prefs = context.getSharedPreferences(SHARED_PREFS_FILE, Context.MODE_PRIVATE);
		Editor editor = prefs.edit();


		persistMenus(context);
		persistHomeItems(context);
		editor.putString(SHARED_PREFS_APP_TITLE_KEY, loginModel.getTitle());
		editor.putString(SHARED_PREFS_USER_NAME_KEY, this.userName);
		editor.putString(SHARED_PREFS_USER_PASS_KEY, this.password);
		editor.putString(SHARED_PREFS_DATEFORM_KEY, loginModel.getDateform());
		editor.putString(SHARED_PREFS_MAIL_DOMAIN, loginModel.getEmailDomain());
		editor.putString(SHARED_PREFS_ZONE_KEY, loginModel.getZone_name());
		editor.putString(SHARED_PREFS_ZONE_VALUE, loginModel.getTimezone());

		editor.putString(SHARED_PREFS_USER_CN_KEY, loginModel.getU_cn());
		editor.putString(SHARED_PREFS_USER_NO_KEY, loginModel.getU_no());

		editor.putString(SHARED_PREFS_COOKIE_KEY, loginModel.getCookie());
		editor.putString(SHARED_PREFS_HMAILKEY_KEY, loginModel.getHmail_key());
		editor.putString(SHARED_PREFS_DOMAIN_KEY, domain);

		editor.putBoolean(SHARED_PREFS_LOGIN, this.logIn);
		editor.putInt(SHARED_PREFS_NOTE_WIDTH, this.noteWidth);
		editor.putInt(SHARED_PREFS_NOTE_HEIGHT, this.noteHeight);
        editor.putBoolean(SHARED_PREFS_CLOUD_VALUE, loginModel.isCloud());
		editor.commit();
	}

	private void persistHomeItems(Context context){
		HomeItemsAdapter adapter = new HomeItemsAdapter(context);
		adapter.open();
		adapter.deleteAll();
		if(loginModel != null && loginModel.getData() != null){
			Iterator<HomeItemModel> iterator = loginModel.getData().iterator();
			while(iterator.hasNext()){
				HomeItemModel itemModel = iterator.next();
				adapter.insertHomeItem(itemModel.getName(), itemModel.getNum(), itemModel.getTitle(), itemModel.isShow());
			}
		}
		adapter.close();
	}


	private void persistMenus(Context context){
		MenusAdapter adapter = new MenusAdapter(context);
		adapter.open();
		adapter.deleteAll();

		if(loginModel != null && loginModel.getMenu() != null){
			for(MenuItemModel item : loginModel.getMenu()){
				adapter.insertCategory(item.getName(), item.getTitle(), new int[]{item.getOffIcon(), item.getOnIcon()});
			}
		}
		adapter.close();
	}


	public String getMailDomain() {
		return loginModel.getEmailDomain();
	}


	private void getHomeItems(Context context){
		HomeItemsAdapter adapter = new HomeItemsAdapter(context);
		adapter.open();

		Cursor homeCurSor = adapter.fetchAllFavorite();

		int keyNameIndex = homeCurSor.getColumnIndex(HomeItemsAdapter.KEY_NAME);
		int keyTitleIndex = homeCurSor.getColumnIndex(HomeItemsAdapter.KEY_TITLE);
		int keyNum = homeCurSor.getColumnIndex(HomeItemsAdapter.KEY_NUM);
		int keyShow = homeCurSor.getColumnIndex(HomeItemsAdapter.KEY_SHOW);

		List<HomeItemModel> homeTabItemModels = new ArrayList<HomeItemModel>();

		int num;
		String name, title;
		int isShow;

		if(homeCurSor.moveToFirst()){
			do{
				HomeItemModel itemModel = new HomeItemModel();

				name = homeCurSor.getString(keyNameIndex);
				num = homeCurSor.getInt(keyNum);
				title = homeCurSor.getString(keyTitleIndex);
				isShow = homeCurSor.getInt(keyShow);
				
				itemModel.setName(name);
				itemModel.setNum(num);
				itemModel.setTitle(title);
				itemModel.setShow((isShow == 1));
				homeTabItemModels.add(itemModel);
			}while(homeCurSor.moveToNext());
		}

		homeCurSor.close();
		adapter.close();

		loginModel.setData(homeTabItemModels);
	}


	private void getMenus(Context context){
		MenusAdapter adapter = new MenusAdapter(context);
		adapter.open();

		Cursor categoryCursor = adapter.fetchAll();

		int keyNameIndex = categoryCursor.getColumnIndex(MenusAdapter.KEY_NAME);
		int keyLabelIndex = categoryCursor.getColumnIndex(MenusAdapter.KEY_LABEL);
		int keyState0 = categoryCursor.getColumnIndex(MenusAdapter.KEY_STATE_0);
		int keyState1 = categoryCursor.getColumnIndex(MenusAdapter.KEY_STATE_1);
		List<MenuItemModel> menus = new ArrayList<MenuItemModel>();
		if(categoryCursor.moveToFirst()){
			do{
				MenuItemModel itemModel = new MenuItemModel();
				itemModel.setName(categoryCursor.getString(keyNameIndex));
				itemModel.setTitle(categoryCursor.getString(keyLabelIndex));
				itemModel.setOffIcon(categoryCursor.getInt(keyState0));
				itemModel.setOnIcon(categoryCursor.getInt(keyState1));
				menus.add(itemModel);
			}while(categoryCursor.moveToNext());
		}
		loginModel.setMenu(menus);
		categoryCursor.close();
		adapter.close();
	}

	public String getTabTitle(String tabKey){

		for(MenuItemModel item : loginModel.getMenu()){
			if(tabKey.equals(item.getName())){
				return item.getTitle();
			}
		}

		return null;
	}



	public void setHomes(HomeModel homeModel){
		if(loginModel != null)
			loginModel.setData(homeModel.getData());
	}



	public int getNoteWidth() {
		return noteWidth;
	}


	public void setNoteWidth(int noteWidth) {
		this.noteWidth = noteWidth;
	}


	public int getNoteHeight() {
		return noteHeight;
	}


	public void setNoteHeight(int noteHeight) {
		this.noteHeight = noteHeight;
	}


	public String getUserId() {
		return userName;
	}

	public void setUserId(String stringstrUserId) {
		this.userName = stringstrUserId;
	}

	public String getStrAppTitle() {
		return loginModel.getTitle();
	}


	public String getPassword() {
		return password;
	}

	public void setPassWord(String strUserPass) {
		this.password = strUserPass;
	}

	public String getDate() {
		return loginModel.getDate();
	}

	public String getTime() {
		return loginModel.getTime();
	}

    public String getDateForm(){
        return loginModel.getDateform();
    }

	public List<HomeItemModel> getHomeData() {
		return loginModel.getData();
	}



	public String getCookie() {
		return loginModel.getCookie();
	}



	public String getDomain() {
		return domain;
	}


	public void setDomain(String strDomain) {
		domain = strDomain;
	}


	public String getHmailKey() {
		return loginModel.getHmail_key();
	}


	public boolean isLogIn() {
		return logIn;
	}


	public void setLogIn(boolean logIn) {
		this.logIn = logIn;
	}


	public String getUserCn() {
		return loginModel.getU_cn();
	}



	public String getUserNo() {
		return loginModel.getU_no();
	}



	public String getZoneName() {
		return loginModel.getZone_name();
	}


	public String getTimezone() {
		return loginModel.getTimezone();
	}



	public List<MenuItemModel> getMenus() {
		return loginModel.getMenu();
	}

    public int indexOfMenu(String key){
        if(loginModel.getMenu() != null){
            MenuItemModel itemModel = new MenuItemModel();
            itemModel.setName(key);
            return loginModel.getMenu().indexOf(itemModel);
        }
        return -1;
    }


    public  boolean isCloud(){
        return loginModel.isCloud();
    }

    public String getLang(){
        return loginModel.getLang();
    }

}
