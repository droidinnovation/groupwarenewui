package com.hanbiro.groupwaredemo.email.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.hanbiro.groupwaredemo.model.ISearchListItem;

import org.apache.commons.lang.StringEscapeUtils;

import java.io.Serializable;

public class MailListItemModel implements Serializable, ISearchListItem, Parcelable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String addr;
	private String subject;
	private String date;
	private String mid;
	private int isnew;
	private String acl;
    private String from_addr;
    private String from_name;
	private String start;
	private String block;
	private String dummy;
	private String basicBlock;
	private int is_file;
	private boolean isChecked;
	private boolean isSelected;


//    public MailListItemModel(ArticalItemModel item){
//        if(item != null)
//            this.mid = item.getDocuId();
//    }
	
	@Override
	public String getData1() {
		return getAddr();
	}
	@Override
	public String getData2() {
		return getDate();
	}
	

	public String getSubject() {
		return StringEscapeUtils.unescapeHtml(subject);
	}
	
	public String getAddr() {
		return StringEscapeUtils.unescapeHtml(addr);
	}
	public void setAddr(String addr) {
		
		this.addr = addr;
	}
	
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getMid() {
		return mid;
	}
	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getAcl() {
		return acl;
	}
	public void setAcl(String acl) {
		this.acl = acl;
	}
	public String getStart() {
		return start;
	}
	public void setStart(String start) {
		this.start = start;
	}
	public String getBlock() {
		return block;
	}
	public void setBlock(String block) {
		this.block = block;
	}
	public String getDummy() {
		return dummy;
	}
	public void setDummy(String dummy) {
		this.dummy = dummy;
	}
	public String getBasicBlock() {
		return basicBlock;
	}
	public void setBasicBlock(String basicBlock) {
		this.basicBlock = basicBlock;
	}
	public boolean isChecked() {
		return isChecked;
	}
	public void setChecked(boolean isChecked) {
		this.isChecked = isChecked;
	}
	public int getIsnew() {
		return isnew;
	}
	public void setIsnew(int isnew) {
		this.isnew = isnew;
	}
	public boolean isSelected() {
		return isSelected;
	}
	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}
	public int getIs_file() {
		return is_file;
	}
	public void setIs_file(int is_file) {
		this.is_file = is_file;
	}

    public String getFrom_name() {
        return from_name;
    }

    public void setFrom_name(String from_name) {
        this.from_name = from_name;
    }


    public String getFrom_addr() {
        return from_addr;
    }

    public void setFrom_addr(String from_addr) {
        this.from_addr = from_addr;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.from_addr);

        dest.writeString(this.addr);
        dest.writeString(this.subject);
        dest.writeString(this.date);
        dest.writeString(this.mid);
        dest.writeInt(this.isnew);
        dest.writeString(this.acl);
        dest.writeString(this.from_name);
        dest.writeString(this.start);
        dest.writeString(this.block);
        dest.writeString(this.dummy);
        dest.writeString(this.basicBlock);
        dest.writeInt(this.is_file);
        dest.writeByte(isChecked ? (byte) 1 : (byte) 0);
        dest.writeByte(isSelected ? (byte) 1 : (byte) 0);
    }

    public MailListItemModel() {
    }

    private MailListItemModel(Parcel in) {
        this.from_addr = in.readString();
        this.addr = in.readString();
        this.subject = in.readString();
        this.date = in.readString();
        this.mid = in.readString();
        this.isnew = in.readInt();
        this.acl = in.readString();
        this.from_name = in.readString();
        this.start = in.readString();
        this.block = in.readString();
        this.dummy = in.readString();
        this.basicBlock = in.readString();
        this.is_file = in.readInt();
        this.isChecked = in.readByte() != 0;
        this.isSelected = in.readByte() != 0;
    }

    public static final Parcelable.Creator<MailListItemModel> CREATOR = new Parcelable.Creator<MailListItemModel>() {
        public MailListItemModel createFromParcel(Parcel source) {
            return new MailListItemModel(source);
        }

        public MailListItemModel[] newArray(int size) {
            return new MailListItemModel[size];
        }
    };
}
