package com.hanbiro.groupwaredemo.database;



import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.commonsware.cwac.loaderex.acl.SQLiteCursorLoader;

public class DBIdAdapter {
	public static final String TAG = "DatabaseCategoryAdapter";
	
	
	public static final String TABLE = "user";
	public static final String KEY_ROWID = "_id";
	public static final String KEY_NAME = "name";

	public static final String FECHALL_SELECT = "SELECT " + KEY_ROWID + " ," + KEY_NAME
			+ " FROM " + TABLE;

	public static final String CHECK_EXIST_QUERY = "SELECT 1 from " + TABLE + " where " + KEY_NAME + " =%s";
	
	private Context m_Context;
	private DataBaseHelper m_DBHelper;
	private SQLiteDatabase m_Database;

	public DBIdAdapter(Context context) {
		m_Context = context;
	}

	public synchronized DBIdAdapter open(){
		if(m_DBHelper == null){
			m_DBHelper = DataBaseHelper.getInstance(m_Context.getApplicationContext());
		}

		m_Database = m_DBHelper.getWritableDatabase();
		return this;
	}

	public void deleteAll()
	{
		m_Database.delete(TABLE, null, null);

	}

	public void close(){
		//m_Database.close();
		//m_DBHelper.close();
	}

	public boolean exist(String id) {
		Cursor cursor = m_Database.rawQuery("select 1 from " + TABLE + " where " + KEY_NAME + " =?", new String[] {id});
		boolean exists = (cursor.getCount() > 0);
		cursor.close();
		return exists;
	}

	public void insertIfNotExist(String id){
		if(!exist(id)){
			ContentValues values = createValue(id);
			m_Database.insert(TABLE, null, values);
		}
	}


	public void insertIdItem(String id){
		try{
			ContentValues values = createValue(id);
			m_Database.insert(TABLE, null, values);
		}catch (android.database.sqlite.SQLiteConstraintException se){
			//se.printStackTrace();
		}
	}

	public SQLiteCursorLoader fetchAllViaCursorLoader(){
		return new SQLiteCursorLoader(m_Context, m_DBHelper, FECHALL_SELECT, null);
	}

	public Cursor fetchAllFavorite() {		
		Cursor cursor = m_Database.query(TABLE, new String[] {KEY_ROWID, KEY_NAME}, null, null, null, null, null);
		cursor.moveToFirst();
		return cursor;
	}

	public Cursor fetchId(long rowID) {

		Cursor cursor = m_Database.query(true, TABLE, new String[] { KEY_NAME}, KEY_ROWID + " = " + rowID, null, null, null, null, null);		
		if(cursor != null){
			cursor.moveToFirst();
		}

		return cursor;
	}

	public boolean deleteId(long rowId) {

		return m_Database.delete(TABLE, KEY_ROWID + "=" + rowId, null) > 0;
	}

	private ContentValues createValue(String id){
		ContentValues value = new ContentValues();
		value.put(KEY_NAME, id);
		return value;
	}
}
