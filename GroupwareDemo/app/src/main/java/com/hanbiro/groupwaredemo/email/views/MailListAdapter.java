package com.hanbiro.groupwaredemo.email.views;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.hanbiro.groupwaredemo.C;
import com.hanbiro.groupwaredemo.R;
import com.hanbiro.groupwaredemo.email.models.MailBoxItemModel;
import com.hanbiro.groupwaredemo.email.models.MailListItemModel;

import java.util.List;
import java.util.Map;

public class MailListAdapter extends RecyclerView.Adapter<MailListAdapter.Holder> {

    private List<MailListItemModel> items;

    private LayoutInflater inflater;

    public MailListAdapter(List<MailListItemModel> data, Context context) {
        items = data;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public Holder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.mail_list_item, viewGroup, false);

        return new Holder(itemView);
    }

    @Override
    public void onBindViewHolder(Holder holder, int i) {
        MailListItemModel itemModel = items.get(i);

        String from = itemModel.getFrom_name();
        if (from == null || from.trim().length() < 1) {
            from = itemModel.getAddr();
        }

        if (from == null || from.trim().length() < 1) {
            from = itemModel.getFrom_addr();
        }
        String icon = "G";
        if (from != null) {
            from = from.trim();
            if (from.length() > 1) {
                icon = from.substring(0, 1).toUpperCase();
            }
        }

        holder.tvSubject.setText(itemModel.getSubject());
        holder.tvIcon.setText(icon);
        holder.tvFrom.setText(from);
        holder.tvTime.setText(itemModel.getDate());

        if (itemModel.getIs_file() == 1) {
            holder.ivAttach.setVisibility(View.VISIBLE);
        } else {
            holder.ivAttach.setVisibility(View.GONE);
        }

        if (itemModel.getIsnew() == 1) {
            holder.tvSubject.setTypeface(null, Typeface.BOLD);
            holder.tvFrom.setTypeface(null, Typeface.BOLD);
        } else {
            holder.tvSubject.setTypeface(null, Typeface.NORMAL);
            holder.tvFrom.setTypeface(null, Typeface.NORMAL);
        }

    }

    @Override
    public int getItemCount() {
        return (items == null) ? 0 : items.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        public Holder(View view) {
            super(view);

            tvSubject = (TextView) view.findViewById(R.id.tvSubject);
            tvIcon = (TextView) view.findViewById(R.id.tvIcon);
            tvTime = (TextView) view.findViewById(R.id.tvTime);
            tvFrom = (TextView) view.findViewById(R.id.tvFrom);
            ivAttach = view.findViewById(R.id.ivAttach);
        }

        public TextView tvSubject;
        public TextView tvIcon;
        public TextView tvTime;
        public TextView tvFrom;
        public View ivAttach;
    }


}
